#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Equipo Umbela'
SITENAME = 'Umbela TS'
#SITESUBTITLE = "Maneras de crear un nosotros, un equipo, una comunidad, una red, para tejer en conjunto caminos hacia una Tierra más sostenible."
DESCRIPTION = "Maneras de crear un nosotros, un equipo, una comunidad, una red, para tejer en conjunto caminos hacia una Tierra más sostenible."
KEYWORDS = "transformación, sostenibilidad, ambientales, caminos, transformador, tierra, comunidad, equipo, red"
SITEURL = 'http://localhost:8000'


DISPLAY_PAGES_ON_MENU = True

PATH = 'content'

TIMEZONE = 'Mexico/General'

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


DEFAULT_PAGINATION = 3

RELATIVE_URLS = False

SUMMARY_MAX_LENGTH = 40

THEME = 'umbelastrap'


PAGE_URL = '/{slug}/'
PAGE_SAVE_AS = '{slug}/index.html'

ARTICLE_URL = '/blog/{slug}/'
ARTICLE_SAVE_AS = 'blog/{slug}/index.html'
ARTICLE_ORDER_BY = 'reversed-date'

PLUGIN_PATHS = ["plugins", ]
PLUGINS = ["test", "carrusel", ]

TEMPLATE_EXTENSIONS = ['.html']

INDEX_SAVE_AS = 'blog/index.html'

STATIC_PATHS = ['media', ]

