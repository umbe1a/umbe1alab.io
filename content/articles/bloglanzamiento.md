---
date: 2022-10-14
title: Lanzamiento del proyecto Umbela
category: proyectos
title_image: lanzamiento.png
image: https://umbela.org/media/lanzamiento.png
author: Paola García-Meneses, Shiara González-Padrón, Beth Tellman, Abril Cid-Salinas, Lakshmi Charli-Joseph, Patricia Pérez-Belmont
english:
---


### Explorando nuevos espacios para transformaciones sostenibles: el proyecto Umbela

### Blog

----

_UN ESPACIO DE ENCUENTRO Y CELEBRACIÓN_

El pasado 4 mayo del 2022 tuvimos un encuentro virtual para compartir y celebrar nuestro nuevo proyecto de ONG Umbela Transformaciones Sostenibles. A lo largo de este evento compartimos qué nos motivó para crear juntas una organización que busca cuestionar y explorar diversas formas de entender el mundo y de crear espacios para abordar problemas socioambientales. En este encuentro también pudimos conversar sobre nuestras experiencias tratando de aterrizar nuestros enfoques teórico-metodológicos en proyectos que buscan transformaciones hacia la sostenibilidad.
<br>


_MOTIVACIONES QUE GENERARON LA CREACIÓN DE UMBELA_

En la actualidad estamos enfrentando retos socioambientales multidimensionales arraigados en dinámicas históricas de inequidad e injusticia. Estos retos, también llamados problemas de sostenibilidad, se caracterizan por su complejidad, falta de consenso en las causas que los originan, así como en la diversidad de comunidades que se dedican e involucran en su estudio. Debido a su complejidad, los problemas socioambientales no han sido abordados adecuadamente. Esto se debe a que la atención a los problemas socioambientales suponen acciones que, por un lado cuestionen el status quo y los modelos de desarrollo convencionales y por el otro, permitan la inclusión de formas alternativas de soluciones. Los problemas de sostenibilidad se han tratado de abordar desde la academia, sector público, social, e incluso privado, pero todavía no es suficiente ya que existen limitantes de índole teórico, metodológico y práctico. Si bien reconocemos a los grupos académicos que atienden las necesidades de la participación e inclusión de actores diversos en los proyectos de investigación, todavía sigue prevaleciendo una práctica extractivista en términos de los alcances e implicaciones de su investigación; y se siguen abordando a las comunidades como objeto de los proyectos y no como sujetos o agentes de cambio.

Bajo estas premisas nos preguntamos ¿qué tipo de diálogos e iniciativas pueden crear espacios de colaboración para incluir a comunidades de práctica clave en los proyectos de sostenibilidad? Nuestra experiencia nos lleva a reconocer que en la oferta de proyectos actuales no se cuestiona lo suficiente sobre a quién o qué se incluye y excluye en la construcción de conocimientos y en el diseño de acciones para la atención de problemas de sostenibilidad.

Nosotras tenemos estas inquietudes, preocupaciones, experiencias, y vivencias que nos han llevado a cuestionamientos profundos acerca de las formas de relacionarnos entre nosotras y otras especies y con los espacios que habitamos. Estos cuestionamientos han ido creciendo con el tiempo, y nos han hecho unirnos para buscar nuevas formas de construir relaciones y significados en torno a la sostenibilidad y justicia socioambiental. Las motivaciones personales se desencadenan de nuestras historias de vida, experiencias profesionales y personales, emociones y pensamientos en donde queremos generar y/o detonar cambios. Todo esto nos hace sentir una co-responsabilidad para buscar activamente la manera de contribuir desde la pluralidad. En nuestras trincheras pasadas nos sentimos de alguna manera restringidas y con pocas libertades para ser más críticas, más alternativas, más desafiantes… es por ello que decidimos formar Umbela.

Sin embargo, estas inquietudes no son únicas pues se comparten con las personas amigas y colaboradoras que estuvieron presentes en el evento. La siguiente nube de palabras ilustra algunas emociones y pensamientos que se compartieron cuando hablamos de los problemas socioambientales. En el centro podemos ver que las palabras más frecuentes son esperanza y ansiedad. Estas emociones -ambas, y en un sentido, opuestas- son parte de la energía que incide y sostiene las transformaciones. En el contexto actual, donde como humanidad transitamos de la esperanza a la ansiedad y frustración de forma constante, creemos profundamente en la importancia de promover mayor sentido de comunidad y de empatía. 

<center>
<img src="/media/nube_palabras.jpg" width="100%"/>
Figura 1. Nube de palabras creada con las respuestas de la audiencia sobre su sentir ante las problemáticas socio-ambientales.
</center>

_¿QUIÉN ES UMBELA?_

Umbela está conformada por seis mujeres con diferentes orígenes geográficos (México, Estados Unidos y Venezuela), con una trayectoria profesional y académica en el campo de las ciencias de la sostenibilidad (te invitamos a ver el <a href="https://www.youtube.com/watch?v=Opk4FNe-_xI&t=3438s">video</a> en donde Abril, Patricia, Lakshmi, Beth, Shiara, y Paola contamos brevemente algunas inspiraciones del por qué crear Umbela).

_LA LABOR DE UMBELA_

Sabemos por experiencias de investigación con diversos sectores (académicos, gubernamentales, privados, sociedad civil) que uno de los principales retos que ocurren en cualquier colaboración parte de construir lo común desde diferentes visiones, lenguajes y prácticas. En Umbela no estamos exentas de esta complejidad; sin embargo, ser una asociación civil (ONG) nos da un poco más de libertad para explorar esquemas de trabajo más horizontales, fomentando ante todo la investigación-acción-participativa, la co-creación de conocimientos y el aprendizaje mutuo entre diferentes comunidades de práctica. Por estas razones, en Umbela quisiéramos contribuir con proyectos en los que podamos explorar e implementar enfoques teórico-analíticos con actividades como: a) el desarrollo de herramientas analíticas de sistemas socio-ambientales; b) el diseño de espacios y procesos participativos con diversas comunidades de práctica; c) la implementación de técnicas de facilitación y de diálogos de saberes; d) el diseño de herramientas de aprendizaje para el fortalecimiento de capacidades; y e) la integración de datos socioambientales multiescalares (Figura 2).

<center>
<img src="/media/actividades_y_capacidades.png" width="80%"/>

Figura 2. Actividades y capacidades de Umbela Transformaciones Sostenibles A.C.
</center>
<br>
Para trabajar de esta manera, es muy importante para nosotras contar con redes de colaboración que estén interesadas como nosotras, en estos esquemas de trabajo; es decir, personas y organizaciones con quienes pongamos en práctica los enfoques que proponemos y que presentamos a continuación. Dado que las transformaciones hacia la sostenibilidad implican procesos complejos y únicos, en Umbela nos enfocamos en el cómo de estos procesos, como parte de nuestra esencia. Esta esencia está basada en explorar cómo se podrían diseñar, facilitar, e implementar estos procesos de cambio, a través de espacios plurales y empáticos. Por lo tanto, una labor constante que hemos llevado a cabo desde que creamos Umbela es aprender de ciertos enfoques que nos inspiran, a la vez que vamos reflexionando sobre ellos y delineando nuestro quehacer. Los cuatro enfoques son:

- <a href="https://umbela.org/enfoques/transdisciplina/">Transdisciplina</a>. En Umbela un enfoque transdisciplinario implica no sólo trascender la noción de disciplinas, sino crear espacios de involucramiento definidos por las comunidades de práctica que co-construyen tanto los objetivos, como el camino a seguir, y los alcances e impacto deseado. Por lo tanto el foco está en las relaciones entre sujetos y en las experiencias de aprendizaje que emanan del proceso relacional.

- <a href="https://umbela.org/enfoques/aprendizaje-transgresivo/">Aprendizaje transgresivo</a>. Ya que las experiencias de aprendizaje son tan centrales para los procesos de transformación, en Umbela queremos ahondar sobre qué condiciones, atmósferas, y actividades resultan más profundas para aprender en conjunto. El aprendizaje transgresivo es una forma de aprendizaje colectivo orientado hacia la acción, y que busca el empoderamiento a través de maneras más reflexivas de conocer. Este enfoque implica plantear formas más radicales de cuestionar y representar lo que podrían significar las transformaciones hacia la sostenibilidad en diversos contextos, y desafiando lo que se ha normalizado (i.e., status quo). Por lo tanto, es una pedagogía disruptiva que pretende abrir otro tipo de espacios en donde el pensar, sentir, y hacer surgen de las experiencias y contextos particulares de los participantes. Les invitamos a explorar nuestro primer producto sobre aprendizaje transgresivo: <a href="https://umbela.org/blog/el-aprendizaje-transgresivo-como-un-catalizador-de-cambio-profundo/">un librito virtual llamado El viaje de Ronin -Una experiencia de aprendizaje transgresivo- y un blog que escribimos para introducirlo</a>. 

- <a href="https://umbela.org/enfoques/innovacion-disruptiva/">Innovación inclusiva</a>. El enfoque de innovación inclusiva lo hemos ido co-construyendo pues creemos que los conceptos de innovación existentes no logran capturar lo que requiere una transformación sostenible. Los conceptos más usados son por ejemplo innovación disruptiva que se enfoca en 'alterar' un mercado existente para transformar el cómo los consumidores interactúan con un producto (como las plataformas digitales de transporte fueron disruptivas para los sistemas de transporte público, y el uso de taxis). Un concepto más utilizado en el Sur Global es el de Jugaad en Hindú, o innovación frugal, que se enfoca en innovaciones para hacer productos más accesibles, de bajo costo, simples y con materiales existentes, para así incrementar el acceso de poblaciones marginadas e históricamente excluidas a oportunidades y a capital socioeconómico. Aunque exploramos estos enfoques, sentimos que estos conceptos son insuficientes para detonar o contribuir a transformaciones hacia la sostenibilidad. Por lo tanto, a partir de la interacción con beneficiarios del proyecto de instalación de Sistemas de Captación de Agua de Lluvia, encontramos una definición en la voz de Bertha Soto González a quien le preguntamos en un podcast en producción sobre captación de agua de lluvia en la Ciudad de México ¿cómo define la innovación que considera inclusiva? 
<center>
<iframe src="https://archive.org/embed/bertha-innovacion" width="500" height="140" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
</center>
Más que una definición de innovación inclusiva, elegimos utilizar descriptores que vienen de los campos de conocimiento de innovación frugal, de base, y comunitaria. Así, para Umbela la innovación inclusiva produce soluciones simples, accesibles y de bajo costo que ofrecen una nueva forma más sostenible de acceder al Buen Vivir (El Sumak Kawsay- Quechua). Además, se incluyen innovaciones creadas en colectivo, con recursos y materiales existentes, sociales y que son transgresivas porque desafían el poder dominante. 

- <a href="https://umbela.org/enfoques/descolonizacion/">Decolonialidad</a>. El enfoque de decolonialidad es el que más hemos explorado con nuestro primer proyecto <a href="https://umbela.org/cultivando-saberes/">_“Cultivando formas de conocer para fomentar transformaciones”_</a>, en el que nos embarcamos en un proceso de buscar cómo fomentar las transformaciones hacia la sostenibilidad desde una mirada más plural. A través de conversaciones con personas que trabajan en América Latina exploramos preguntas como ¿qué significa retar y cuestionar las formas de generar conocimientos?, ¿cómo fomentar caminos hacia la comunalidad?, ¿cómo deconstruir nuestra mente y nuestros quehaceres para ser más plurales y dar apertura a la transformación?, ¿cuáles son los principios que deberían prevalecer en los procesos y espacios colectivos? Estas son algunas de las preguntas que nos permitieron tocar algunas de las aristas de la decolonialidad. Las conversaciones fueron enmarcadas en una metáfora que llamamos el Policultivo de Saberes, con la que exploramos cuatro fases del cultivo conectadas de alguna manera con el enfoque de decolonialidad. Te invitamos a ver un breve <a href="https://vimeo.com/705610771">video</a> que captura las principales ideas que surgieron de estas conversaciones.
<br>

Estos enfoques representan para nosotras un faro, o una brújula que guía nuestro quehacer en Umbela como articuladoras de diversas voces, para que cuando diseñemos e implementemos proyectos de sostenibilidad, podamos cuestionar si de verdad estamos fomentando que estos componentes vayan tomando vida y creando una práctica transformadora más significativa. Durante el tiempo que hemos construido Umbela, hemos dialogado para encontrar descriptores que nos ayuden a poner en práctica nuestros enfoques, encontrando también algunos puntos de convergencia que se pueden leer en la siguiente figura:
<center>
<img src="/media/Articulación_de_enfoques.png" width="100%"/>
Figura 3. En el diagrama encontramos en el centro la pluralidad y el cuidado de los procesos relacionales; y muy cerca a estos, se encuentran otros descriptores que se comparten al menos entre dos enfoques.
</center>

Para nosotras es muy importante generar redes de colaboración con personas a las que consideramos aliados cognitivos, es decir, que quisieran explorar junto con nosotras nuestros enfoques y cómo ponerlos en práctica. Durante el evento hicimos un breve ejercicio para conocer la perspectiva de las personas invitadas sobre en dónde se posicionan dentro de estos conceptos para promover transformaciones sostenibles (Figura 4). Vimos que la mayoría de las personas se identificaron con el enfoque de Transdisciplina, pero casi siempre en conjunto con otro enfoque, como el de Decolonialidad y el de Innovación Inclusiva. Otras personas pusieron su punto en medio y luego explicaron sus sentires y la importancia de cuidar procesos relacionales.

<center>
<img src="/media/Puntos_Enfoques.jpg" width="100%"/>
Figura 4. Posicionamientos de la audiencia conectada por Zoom y YouTube en vivo el día del lanzamiento dentro de las esferas conceptuales-metodológicas presentadas por Umbela.
</center>

----

_SENTIPENSARES DEL EVENTO_

Compartimos a continuación algunos extractos de los sentipensares que las personas invitadas y la audiencia nos transmitieron al final del evento. Después de haber recorrido puntos importantes e interactuado con quienes nos acompañaron en el lanzamiento, queríamos saber qué pensaban y sentían, y reflexionar en conjunto. 

- _La crisis de la pandemia llevó a esfuerzos de pensar otras formas, de trabajar colectivamente._
- _Conversaciones y preguntas estimulantes intelectual y emotivamente, explorando formas alternativas como la poesía._
- _Importancia de aprender sobre la coherencia entre lo que queremos alcanzar y las formas de hacerlo, las formas de trabajar en conjunto, de forma respetuosa, pausada, reconociendo saberes y conocimientos, pero también los tiempos y formas; generando espacios de largo plazo porque las transformaciones y las formas transgresivas de hacer no son a corto plazo._
- _Admirable mantener un rigor y una formalidad dentro del “caos” de los diferentes enfoques y abordajes._
- _Al destilar estas imágenes y metáforas, han logrado un pensamiento muy riguroso, están mostrando lo que puede lograr el “scholar-activism”, saliendo de un movimiento más amplio, desde el patriarcado, colonialismo, modernidad, capitalismo... pensando en las resistencias, pero también en las alternativas…_
- _Celebro la comunidad amorosa y reflexiva, considero que están ”aventadas”, en el sentido de aventar y aventurar en un vuelo. Debemos construir tejidos de afinidades, de aprendizajes mutuos, desde un lugar de escucha y disposición a salir de las zonas de comodidad; celebrar el cuidado afectuoso, el desborde creativo y la fuerza disruptiva transformadora en los desiertos académicos, la iniciativa de Umbela es esa agua que requieren los desiertos académicos._
- _Destaco el poder de enfocarse en el proceso de la colaboración y de la producción de los elementos esenciales para la transformación, las partes afectivas, relacionales, la creatividad, dentro de las relaciones humanas y no humanas, es especial la forma en la que se presentan las ideas._
- _Es muy poderoso transgredir, ir más allá de las fronteras, de lo que está compartimentalizado, fragmentalizado, se requieren espacios para desarmar lo estanco y en esa aventura está la posibilidad de un espacio cuidado y seguro porque los procesos creativos son muy caóticos e inquietantes, que Umbela busque espacios seguros y cuidados, ahí donde está la coherencia._
- _Umbela es una casa para nómadas académico-activistas como yo, es un espacio para explorar los límites de la imaginación para promover algún tipo de transformación._

Umbela arranca con este sueño de seis mujeres con diferentes formas de ver la vida pero que se unen para aprender entre ellas y con otras personas. Sabemos que la ruta que hemos escogido es retadora pero al mismo tiempo fascinante e inspiradora. Estamos muy emocionadas de los nuevos procesos de profundo aprendizaje que Umbela podría gestar y desde una honesta intención de que todes podamos florecer juntes. 

Les invitamos a ver la grabación completa de este evento en nuestro <a href="https://www.youtube.com/watch?v=Opk4FNe-_xI">canal de YouTube</a> ¡Gracias a todas las personas que nos acompañaron!

----
