---
date: 2022-04-29
title: El aprendizaje transgresivo como un catalizador de cambio profundo
category: proyectos
title_image: portada_tinyb.png
image: https://umbela.org/media/portada_tinyb.png
author:  Almendra Cremaschi, Lakshmi Charli-Joseph, Florencia Indira Ciocchini
english:
---

### Nuevas formas de comunicar y aprender: el aprendizaje transgresivo como un catalizador de cambio profundo

### Blog + librito


<br>

El tiempo que vivimos requiere nuevas formas de aprender, de desaprender y de vincularse con otres seres de la Tierra (humanes y no-humanes). Una de estas formas de aprendizaje se llama aprendizaje transgresivo. Esta perspectiva surge a partir del reconocimiento de la necesidad de ir más allá de la creación de conceptos, explicaciones y teorías falsamente abstractas si lo que queremos es apoyar cambios radicales. Desde este enfoque, transgredir significa transgredirnos, romper nuestros disfraces de seres racionales, pensantes y poner luz sobre nuestra esencia sentipensante. Sería, pues, ingenuo decir que este es un proceso cómodo y lineal. Por el contrario, el aprendizaje transgresivo como práctica y como mirada, es una invitación a salir de la zona conocida, a romper las barreras disciplinares, culturales, especistas y de género, para aventurarnos unes a otres en un viaje oscuro, pero siempre acompañado a lo desconocido. 

El aprendizaje transgresivo es un proceso no lineal, atravesado por la incertidumbre de un destino incierto, pero con la liviandad de haber reconocido, y si tenemos fortuna, abandonado pesadas valijas de condicionamientos. El aprendizaje transgresivo es un proceso de desvelo en todos los sentidos. Implica el insomnio y la angustia de un camino sin destino certero y a la vez implica el ejercicio de correr el velo de los marcos de pensamiento, de las pautas socioculturales y de las limitaciones conceptuales, para ver un poco más de cerca lo que colectivamente definimos como verdades. 

Como proceso relacional, el aprendizaje transgresivo no está exento de conflictos, de tensiones de poder y de pujas por la producción de sentido. Tampoco está exento de las tiranías del tiempo, que pretende uniformizar procesos al compás de reportes, proyectos y  ciclos. En el marco de semejante complejidad, no es un proceso que pueda confiársele a la espontaneidad. Por el contrario, requiere de un cuidadoso diseño, acompañamiento, y evaluación, ayudado por herramientas concretas. Y sin embargo, no hay recetas.

Uno de los aspectos clave que se busca transgredir es la forma y el propósito de comunicar. La vida académica obliga muchas veces a publicar sólo para hacer público, para dejar asentado un trabajo que se hizo, y poder reproducirse dentro de un sistema que no deja de mostrar sus limitaciones. Puede no hablársele a nadie, y sin embargo las formas de evaluación disciplinares avalarían tal producción. De frente a esto, el aprendizaje transgresivo busca ser un puente entre diversos mundos, como un mensajero y un disparador de inquietudes que se abordan de manera colectiva.

Existen diversos ejemplos de procesos de aprendizaje transgresivo en el planeta. Aquí te compartimos algunos: <a href="https://transgressivelearning.org/wp-content/uploads/2018/10/Living-Spiral-Framework.Marco-conceptual-del-espiral-vivo.pdf">"Marco conceptual del espiral vivo"</a>; <a href="https://transgressivelearning.org/2018/11/27/10-lessons-learnt-during-the-t-learning-project-in-colombia/">"10 aprendizajes de un proyecto en Colombia"</a>; <a href="https://www.mdpi.com/2071-1050/12/12/4873">"Capturing Transgressive Learning in Communities Spiraling towards Sustainability"</a>; <a href="https://www.ajol.info//index.php/sajee/article/view/137658">"The Listening Train: A Collaborative, Connective Aesthetics Approach to Transgressive Social Learning"</a>.

----

_Los libritos como praxis transgresora_

Inspiradas por estos componentes del aprendizaje transgresivo, nos embarcamos en relatar una historia breve para ilustrar la esencia de este tipo de aprendizaje en un librito (o Tiny Book en inglés). Los libritos surgieron como una metodología que busca democratizar la co-producción y acceso a los conocimientos. Esta idea se gestó en la reunión "Aulas Vivas" (Junio 2018, en Colombia) coordinada por la red de <a href="https://www.google.com/url?q=https://transgressivelearning.org/2018/08/24/transgressive-learning-through-a-tiny-library/&sa=D&source=docs&ust=1651284984596324&usg=AOvVaw3xeszgKhAq_JOvbz-Iv2X5">"T-Learning"</a> (la red de Aprendizaje Transgresivo), y en colaboración con las redes <a href="https://kalpavriksh.org/our-work/alternatives/acknowl-ej/">"Acknowl-EJ"</a> (la red de Conocimiento Co-producido y Académico-activista para la Justicia Ambiental) y <a href="https://steps-centre.org/about-the-pathways-network/">"Pathways"</a> (la red de Vías de Transformación hacia la Sostenibilidad). 

Estos libritos pueden servir como un vehículo para compartir ideas a través de preguntas que detonan reflexiones, en un proceso de diálogo que permite destilar historias que se plasman de manera muy sintética (ya sea en narraciones escritas o ilustradas). 

Para saber más de esta metodología y cómo hacer libritos te invitamos a revisar los siguientes vínculos que surgen del trabajo coordinado por <a href="https://steps-centre.org/about-the-pathways-network/">"Dylan McGarry: El aprendizaje transgresivo a través de una mini biblioteca"</a>; <a href="https://transgressivelearning.org/2018/08/24/transgressive-learning-through-a-tiny-library/">"Libritos para el aprendizaje transgresivo"</a>. Además, en esta <a href="https://steps-centre.org/wp-content/uploads/2019/09/Gui%CC%81a-T-Lab-Xochi-Descargable-versio%CC%81n-pantalla-Espan%CC%83ol-Hub-NA.pdf">"guía"</a> (pag. 32) se muestra también la aplicación de los libritos en un proyecto específico en México y se brindan detalles de cómo hacerlos.


<div class="row">
<div class="col-md-6">>
<img src="/media/libritos_guia.png" width="100%"/>
</div> 
<div class="col-md-6">>
<img src="/media/plantilla_libritos.png" width="100%" />
</div>
</div>


----
_Nuestra experiencia_

Buscando inspirar otras formas de hacer y pensar, y mientras exploramos esta metodología de creación y comunicación de una historia que se va destilando, las tres autoras nos aventuramos en un proceso de diálogo colectivo para crear un librito propio. Teniendo en cuenta que el aprendizaje transgresivo parte de lo personal, nuestras conversaciones tocaron varios temas que nos inspiran a cada una en particular. Por ejemplo, el de la diversidad de lenguajes (de idioma, visual, simbólico-metafórico, etc.) como llave para abrir puertas a otras y nuevas comprensiones; o el de la diversidad de cosmovisiones representadas en comunidades de pueblos indígenas, de practicantes de la agroecología, de pracademiques _(persona cuya carrera se extiende más allá de los límites de la academia y la práctica)_; y el del rol de la cuestión de género en la construcción de conocimientos. De todos los temas que atravesaron las pantallas durante las videollamadas, confesamos que no somos expertas en ninguno, tenemos preguntas en lugar de respuestas, y tenemos mochilas de condicionamientos en lugar de propuestas transformadoras. No sabemos qué reacciones disparará este trabajo, pero sabemos que no somos las mismas que al comienzo del proceso, y eso ya lo hace valioso. 

El aprendizaje transgresivo requiere presencia, pero esta experiencia demuestra que no necesariamente requiere presencialidad. El trabajo virtual, aunque de una manera menos profunda, permite conectar personas e ideas de lugares muy diferentes, en interacciones cortas pero frecuentes, para pensar y crear en conjunto en un diálogo que incluye palabras, pero también silencios, dibujos, miradas y risas. Entender al lenguaje como un fluir de mensajes multidireccionales y polimórficos, implica también reconocer todas las limitaciones de la palabra y la gramática, no sólo como modo de comunicación sino como modo de pensamiento. Este librito, cuyo adjetivo _nuestro_ se va expandiendo desde las autoras hasta todas las personas que directa o indirectamente accedan a él, pretende ser una invitación a pluralizar, a transgredir el lenguaje. 

Así, nuestro librito llamado _El viaje de Ronin -Una experiencia de aprendizaje transgresivo-_ cuenta una parte de la historia de Ronin, un personaje que experimenta el aprendizaje transgresivo, sus etapas, sus oscuridades y dulzuras, tanto en lo individual, como en colectividad. Los personajes de esta historia se inspiran en seres de las comunidades latinoamericanas _<a href="https://pueblosoriginarios.com/sur/amazonia/shipibo/ronin.html">"Shipibo-Konibo y Pijao"</a>_. La alusión a estos personajes y símbolos de dichas culturas nos llevó a reflexionar sobre la posible apropiación cultural que ello pudiera parecer. Para nosotras, todas latinoamericanas y orgullosas de nuestras raíces compartidas, esto se debe a una admiración y deseo de honrar diversas cosmovisiones, y no a una apropiación fuera de contexto.

Elegimos a Ronin como personaje, índice y guía de esta historia por dos razones: Ronin es serpiente y es río, encarna en su ser una doble identidad, quizá en puja, quizá en contradicción, como nosotres, con nuestros múltiples sombreros institucionales, roles y sentires en tensión _¿Quiénes somos?_ 

La segunda razón vuelve a llamar la atención a los rincones del lenguaje, y al poder que allí se ejerce. Ronin es aparentemente un nombre neutro en términos de género, así como su ser, que es un río, y una serpiente. _El aprendizaje transgresivo es trans._ Sale de las delimitaciones de un género literario, de un género identitario, no puede ser comprendido con los parámetros del conocimiento dominante, y como tal, es un aprendizaje disidente, no sirve para calmar, para reproducir las lógicas productivistas y positivistas del aprendizaje instituido. 

Es por lo anterior que tomamos a Mohán como sujeto también. Mohán encarna en su identidad la cultura patriarcal, es agresivo y es protector, pero además se vincula a la cultura de la violación, no sólo porque según la narrativa a la que se lo asocia, él mismo ultraja mujeres que van a lavar al río, sino porque la respuesta comunal a ello es que sean acompañadas de un hombre. _El aprendizaje transgresivo es feminista._ No brinda tácticas para la defensa y la adaptación. Pretende derrumbar las narrativas que son raíces de las injusticias cognitivas y ecosociales, pretende desarmar los machos de la retórica cientificista y liberal, pero no con una lógica patriarcal basada en la violencia y en la racionalidad, sino apelando al cuidado y a la deconstrucción sentipensante. 

No sorprenderá entonces, que quien conduce a Ronin en las últimas etapas de su viaje de (auto)conocimiento sea una chamana, que así como él, ha sido desplazada de su comunidad por no encajar. La chamana no sólo empatiza con Ronin sino que se transforma a ella misma en ese vínculo y vuelve al origen. _El aprendizaje transgresivo es empático,_ que ve en la otredad un espejo, que se reconoce en los viajes compañeros aunque no haya recorrido exactamente el mismo camino. Quien se reconozca como parte de un proceso de aprendizaje transgresivo, identificará todos los momentos en que a la confrontación le ganó la pregunta, en que al apuro le ganó la pausa, en que a la urgencia del acuerdo le ganó el cuidado. 

Es así como esta historia-proceso-blog-vínculo busca y se inspira por la totalidad, la síntesis, la celebración de la interdependencia. Es cíclica y circular, como congregación de las propias luces y sombras, de las distintas etapas de la vida. Sus diferentes porciones (conocimientos/sujetos, etc) a su vez se 'abollan' en el encuentro. Abonan y son abonadas por ese centro-semilla. 

Con este librito en versión digital, te invitamos a acompañar a Ronin en su camino de transformación y descubrimiento. Ojalá lo disfrutes y nos cuentes qué opinas.

<iframe src="/media/ronin/El_viaje_de_Ronin_Una_experiencia_de_aprendizaje_transgresivo.pdf" width="100%" height="500px">
 </iframe>



<center>
<img alt="Dibujo circulo" src="/media/dibujo_circular.png" width="40%" />
</center>

