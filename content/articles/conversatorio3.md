---
date: 2021-11-20
title: Conversatorio 3
category: proyectos
title_image: media/Conv3.png
image: https://umbela.org/media/Conv3.png
carrusel: True
author: Mariana Benítez, Patricia Pérez-Belmont, Helda Morales, Aranzazú Díaz, Flor Ciocchini, Luis Bracamontes, Lakshmi Charli-Joseph
english: https://en.umbela.org/blog/conversatory-2/
---

### Cosechando saberes
### Blog + video  

La “Cosecha de saberes” es un conversatorio donde hablamos desde lo colectivo, desde las organizaciones que han sembrado semillas diversas y que con cada ciclo de cultivo han recolectado errores, éxitos y aprendizajes que les han permitido evolucionar. Les presentamos a continuación la narración del cadáver exquisito que Mariana Benítez escribió a partir de la conversación sobre la cosecha de saberes.

<center>
<iframe src="https://archive.org/embed/cadaver-exquisito-conv-3" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
</center>

<center>
<p>
<em>
La siembra, el cultivo y la cosecha están ocurriendo todo el tiempo, 
cada vez que vamos cosechando nuevos aprendizajes van nutriendo la organización, 
la misma cosecha de saberes no es lineal, 
no nos educamos solos, ni una persona a otra, nos educamos juntas, 
cosechando y sembrando, cosechando y sembrando, y mucho se va a morir, 
atmósferas de aprendizaje en donde miramos de iguales, 
lo dialéctico entre lo individual y lo colectivo, a veces disruptivos.

Ahí entra el tiempo, ahí en el descanso resignificamos movimientos muy importantes 
que pueden tener continuidad en otro espacio y en otro tiempo, 
a veces la plaga he sido yo, nosotros con nuestro corre y corre, 
a veces he tenido que prender fuego y sacar cosecha de las cenizas, 
roza, tumba y quema, que he tenido que hacer conmigo misma, 
hay que saber cuando retirarse y dejar crecer.

La vida al borde del abismo, contradicción constante, estar fuera y estar adentro, 
navegando en diferentes condiciones, inclusive la pandemia, 
compartir de organización a organización, nunca se finaliza una obra, siempre está en movimiento, 
no hay recetas, sólo principios, llevar cosecha a través de la cocina, 
la comida nos entusiasma y nos conecta, un aula-cocina-laboratorio, 
avanzamos la agroecología comiéndola, humildad, escucha, saber que todos tenemos algo que aportar, 
mirarnos como milpa, con nuestras diferentes partes que pueden crecer separadas pero mejor juntas, 
ponernos a cocinar, a comer, a trabajar en una obra de teatro o a escribir en un poema.

En la academia ¿qué es lo que vale? realmente va a lograr lo que queremos?, 
la organización inicia con la escoba, asamblea, trabajo, fiesta, 
el encuentro a partir de cocinar, el arte o la fiesta, 
una desnudez para enfrentarnos a los conflictos, la celebración, 
la travesura y algo para compartir, vernos con ojos distintos, 
estar preparadas para desafiar y ser desafiadas, irritar, incomodarnos y mover.
</p>
</em>
</center>

<div style="padding:56.27% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/651264991?h=6fa86ec133&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Cosechando saberes"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

