---
date: 2024-02-08
title: and this is why i need you
category: poema
title_image: connections.png
image: https://umbela.org/media/connections.png
author: Susi Moser
english: https://en.umbela.org/blog/and_this_is_why_i_need_you/
---

<table>
<tr>
<td>
<b>and this is why i need you</b>
</td>
<td>
<b>y por eso te necesito</b>
</td>
</tr>
<tr>
<td valign="top">
<pre style="padding-right: 2em;">
because we’re in it now.
and it is dark. and it is frightening. 
and i can’t see clearly
on my own.

because we’re in it now.
and it is hot. too dry. too wet. 
and i lose sight without you of where we are
and where we’re going.

because alone i cannot find
that reasonable balance
between disappointment and hope.

because you help me build
muscle memory of how change happens. 
by scaling in and scaling out.
you help me see where we are
and what to do.

you show me who we are. 
and who we have been. 
who we may yet become – 
the kind of people
Earth needs now.

because alone i can find 
a safe space.
but only with you
does it become
a brave space.

and i must be brave now. 
because i don’t see
the need for change
shrinking or slowing.
and my zone of influence
is just two arm-lengths worth.

because the seeds i sow
and the seeds you sow
have still low visibility.
and if you wouldn’t help me see them
i’d go crazy.

also because there is peace 
and promise
and power
in our shared silence.

because i am tired and i cannot read 
all that you know.
and yet there is so much
i must learn.
and so much more i need to unlearn.

also because with you
i can remember
that i don’t have to know 
or do everything.
but find my place
and “start where i am. 
do what i can.
use what i have.”
and if you do the same 
it is enough.

because you disrupt 
my old stories
and offer up new ones.

because i had forgotten 
about the prophecy
of how to subvert
the palaces of power 
until you told me 
about trojan mice.

because common sense 
is not yet legislated. 
but at least together
we can cultivate
a shared intent.

and if we don’t get funded 
at least we can pool
the little we have
and still be free.

because you remind me 
that it is precisely
in the obstacles we meet 
that our work lies.

and that together
we are bolder
in our thinking and actions, 
in our connections
and reflections.

because the mirror
you hold up by being you
is precisely the one i need 
to look inside of me.

and because if you hold up 
one end and i the other
of the polarities between us 
something may yet give. 
and something new may yet 
come forth.

because together
we can liberate our minds 
and reimagine our futures. 
reimagine ourselves. 
become indigenous again.

because together we are
tamkeen – the potential in the seed 
meeting the favorable conditions 
for its flourishing.

because together we are
the future – which is not a time 
but a possibility looking back at us 
knowing, intimately,
that it depends on each
one of us.

~ Susi Moser, July 14, 2023 
</pre>

<i>(Poem written in gratitude for every single person 
involved in the Transformations Conference 2023; 
held in Sydney, Australia, Prague, Czech Republic, 
and Portland, Maine, USA; July 11-19, 2023. 
Many of the ideas and phrases in this poem are 
words spoken by participants).</i>
</d>

<td valign="top">
<pre  style="padding-right: 2em;">
porque estamos en ello ahora.
y es oscuro. y es aterrador. 
y no puedo ver claramente
por mi cuenta.

porque estamos en ello ahora.
y hace calor. demasiado seco. demasiado húmedo. 
y sin ti pierdo de vista dónde estamos
y hacia dónde vamos.

porque sola no puedo encontrar
ese equilibrio razonable
entre la decepción y la esperanza.

porque me ayudas a construir
memoria muscular de cómo se produce el cambio. 
reduciendo y ampliando la escala.
me ayudas a ver dónde estamos
y qué hacer.

me muestras quiénes somos. 
y quiénes hemos sido. 
quiénes podemos llegar a ser – 
el tipo de personas
que la Tierra necesita ahora.

porque sola puedo encontrar 
un espacio seguro.
pero sólo contigo
se convierte en
un espacio valiente.

y ahora debo ser valiente. 
porque no veo
que la necesidad de cambiar
disminuya o se frene.
y mi zona de influencia
es de apenas dos brazos.

porque las semillas que siembro
y las semillas que siembras
siguen siendo poco visibles.
y si no me ayudaras a verlas
me volvería loca.

también porque hay paz 
y promesa
y poder
en nuestro silencio compartido.

porque estoy cansada y no puedo leer 
todo lo que sabes.
y sin embargo hay tanto
que debo aprender.
y mucho más que necesito desaprender.

también porque contigo
puedo recordar
que no tengo que saberlo 
ni hacerlo todo.
sino encontrar mi lugar
y "empezar donde estoy. 
hacer lo que pueda.
usar lo que tengo".
y si tú haces lo mismo 
es suficiente.

porque desbaratas 
mis viejas historias
y ofreces otras nuevas.

porque había olvidado 
de la profecía
de cómo subvertir
los palacios del poder 
hasta que me hablaste 
sobre ratones troyanos.

porque el sentido común 
aún no está legislado. 
pero al menos juntos
podemos cultivar
una intención compartida.

y si no conseguimos financiamiento 
al menos podemos poner 
lo poco que tenemos
y seguir siendo libres.

porque me recuerdas 
que es precisamente
en los obstáculos que encontramos 
donde reside nuestro quehacer.

y que en conjunto
somos más audaces
en nuestra forma de pensar y de actuar, 
en nuestros vínculos
y reflexiones.

porque el espejo
que sostienes siendo tú 
es precisamente el que necesito 
para mirar dentro de mí.

y porque si sostienes 
un extremo y yo el otro
de las polaridades entre nosotros 
algo aún pudiera ceder. 
y algo nuevo podría 
también surgir.

porque juntas
podemos liberar nuestras mentes 
y re-imaginar nuestros futuros. 
re-imaginarnos a nosotras mismas. 
volvernos de nuevo nativas.

porque juntos somos
tamkeen – ese potencial en la semilla 
que reúne las condiciones favorables 
para su florecimiento.

porque juntos somos
el futuro – que no es un tiempo 
sino una posibilidad que nos mira 
sabiendo, íntimamente,
que depende de cada
uno y una de nosotras.

~ Susi Moser, 14 de Julio, 2023 
</pre>
<i>(Poema escrito en agradecimiento a cada una 
de las personas que participaron en la Conferencia Transformaciones 2023; 
celebrada en Sidney-Australia, Praga-República Checa, y Portland-Maine-EE.UU.; 11-19, julio 2023. 
Muchas de las ideas y frases de este poema son palabras pronunciadas por los participantes). 
Traducido por Lakshmi Charli-Joseph, con permiso de la autora.</i>

</td>
</tr>
</table>  
