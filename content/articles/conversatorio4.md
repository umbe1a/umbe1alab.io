---
date: 2021-11-20
title: Conversatorio 4
category: proyectos
title_image: media/Conv4.png
image: https://umbela.org/media/Conv4.png
carrusel: True
author: J. Mario Siqueiros-García, Lakshmi Charli-Joseph, Marila Lázaro, María Mancilla-García, Ingrid Estrada y Patricia Pérez-Belmont
english: https://en.umbela.org/blog/conversatory-4/
---

### Cuidando y nutriendo la tierra
### Blog + video  

El tema del cuarto conservatorio ´Cuidando y nutriendo la tierra´ es el cuidado de los procesos-espacios de transformación desde dos sentidos complementarios: 
1.	el de dedicarle la atención, el esmero y el tiempo a diseñar e implementar procesos para posibilitar transformaciones, observando los detalles como asegurar que cada una de las voces participantes sea escuchada, la transparencia del proceso, y la estructura y calidad del diálogo;
2.	el de nutrir los procesos así como quienes cultivan la tierra nutren el suelo, lo que implica mirar profundo y hacer lo necesario para procurar el bienestar de un nosotros transhumano.

El diálogo de este conversatorio discurre entre distintos puntos del paisaje que dibujan estas dos formas de pensar la noción de cuidado. En este espacio sobresalen la belleza, la responsabilidad de cuidar y el derecho a ser cuidado. Rara vez se piensa en la belleza en los procesos de transformación; sin embargo, en este conservatorio se habla de su importancia como un elemento potente de soporte, que motiva y nos engancha con dichos procesos a largo plazo. 

Y también están la muerte, las ruinas, los duelos y las heridas, así como la añoranza por hacer comunidad, por conectar con lo diferente. La muerte y las heridas son motivación para el abrazo colectivo y las ruinas, como la belleza, son soporte, son las marcas en el territorio de su historia, de lo que ha estado antes, de lo que está allí que pasamos por alto, o que muchas veces nos es invisible pero que sin ellas los procesos de transformación en los que nos involucramos no serían posibles.

En este conversatorio también se vuelve al tema de las utopías que se enlazan con la belleza y con el futuro que puede ser y que conectan el aspecto temporal al ser una visión del futuro que incide en nuestro hacer del presente, a la vez que invitan a pensar en el papel de las ruinas en la proyección del futuro… un futuro en donde nos cuidamos, -con un sentido de nosotres más allá de lo humano- para actuar en consecuencia y procurarnos aquello que pueda conducir en esa dirección trascendiendo la agencia de los sujetos.

Te compartimos fragmentos de esta conversación en la que también se resalta el papel de las personas facilitadoras como articuladoras de voces y saberes, papel que debe ser tomado desde una escucha profunda y una humildad genuina para cuidar y nutrir procesos de transformación.

<center>
<img alt="Conversatorio 4" src="/media/conv4garabatos.png" width="80%" />
</center>

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/663792426?h=661f730e02&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Cuidando y nutriendo la tierra.m4v"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
