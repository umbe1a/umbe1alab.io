---
date: 2023-02-02
title: Convocatoria postdoctoral 2023
category: proyectos
title_image: convocatoria_postdoc.jpg
image: https://umbela.org/media/convocatoria_postdoc.jpg
author:  Umbela
english:
---

### Investigación Acción Participativa para co-crear trayectorias de transformación sobre el agua

<br>

<center>
CONVOCATORIA CERRADA
</center>

<br>

<center>
_¿Te interesa diseñar y poner en práctica procesos que fomenten transformaciones sostenibles? ¿Sientes curiosidad por las herramientas y métodos participativos creativos para comprometerte con diversas comunidades de práctica? ¿Te gustaría formar parte de un equipo internacional dedicado a compartir aprendizajes sobre Investigación Acción Participativa en diversos humedales del planeta?_
</center>

<center>
<img src="/media/convocatoria_postdoc.jpg" width="100%"/>
</center>

En el marco del proyecto Water Transformation Pathways Planning (<a href="https://www.un-ihe.org/news/water-and-development-partnership-programme-six-projects-selected-funding">"Trans-Path-Plan"</a>), financiado por el Programa de Cooperación sobre Agua y Desarrollo (<a href="https://www.un-ihe.org/what-we-do/water-and-development-partnership-programme">"the Water and Development Partnership Programme"</a>), un consorcio internacional recientemente constituido, se adentrará en el ámbito de la planeación de trayectorias de transformación en el sector del agua.

El nodo de la Ciudad de México está coordinado por la Dra. Marisa Mazari Hiriart y la Dra. Lakshmi Charli-Joseph del Laboratorio Nacional de Ciencias de la Sostenibilidad (LANCIS) del Instituto de Ecología -UNAM (IE-UNAM), por la Dra. Beth Tellman de la Universidad de Arizona - USA (UofA), por el Dr. Jaime Paneque del Centro de Investigaciones en Geografía Ambiental (CIGA-UNAM) y por Patricia Pérez-Belmont de Umbela Transformaciones Sostenibles (Umbela).

Trabajaremos en el humedal de Xochimilco, un sistema socio-ecológico con un agro-ecosistema altamente productivo, y en su área periurbana, para explorar la diversidad de intereses, necesidades y tensiones, y así entender cómo los esquemas institucionales y de gobernanza determinan ciertos discursos y narrativas en torno a la planeación sostenible. El área de nuestro proyecto incluye el territorio, el agua y las personas que interactúan con la subcuenca que drena hacia el humedal de Xochimilco, considerado Patrimonio de la Humanidad por la UNESCO. Nos acercaremos a varios lugares específicos de este ámbito espacial de interés para co-construir trayectorias de transformación con la población, las organizaciones y las comunidades locales.

Te invitamos a ver más detalles de esta convocatoria: 

<iframe src="/media/convocatoria_postdoc_extensa.pdf" width="100%" height="500px">
</iframe>

