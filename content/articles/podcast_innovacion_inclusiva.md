---
date: 2024-04-10
title: Cosecha de agua de lluvia en CDMX- una innovación inclusiva?
category: podcast
title_image: capturadelluvia_foto.png
image: https://umbela.org/media/capturadelluvia_foto.png
Author: Beth Tellman
Productores: Beth Tellman, <a href= "http://michmorelos.com/">Michelle Morelos </a>  

Apoyo en edición: <a href= "http://michmorelos.com/">Michelle Morelos </a> , Victor Hernández, Nicolas Maillet-Guy
Entrevistados: Enrique Lomnitz, Bertha Soto-Gonzales, Jorge Ortiz-Moreno 
Música de fondo: <a href= "https://soundcloud.com/rmiparks">Robbie M. Parks </a>
Producción: <a href= "http://michmorelos.com/">Michelle Morelos </a>  

---
### Cosecha de agua de lluvia en CDMX- una innovación inclusiva?

---

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1765204080&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/elizabeth-tellman" title="Elizabeth Tellman" target="_blank" style="color: #cccccc; text-decoration: none;">Elizabeth Tellman</a> · <a href="https://soundcloud.com/elizabeth-tellman/captura-de-agua-de-lluvia-en-cdmx-una-innovacion-inclusiva" title="Cosecha de agua de lluvia en CDMX- una innovación inclusiva?" target="_blank" style="color: #cccccc; text-decoration: none;">Cosecha de agua de lluvia en CDMX- una innovación inclusiva?</a></div>

---

En este primer <a href="https://soundcloud.com/elizabeth-tellman/captura-de-agua-de-lluvia-en-cdmx-una-innovacion-inclusiva?si=93e42b4221fc4530b6108f91b352fb59&utm_source=clipboard&utm_medium=text&utm_campaign=social_sharing">podcast</a> de Umbela exploramos distintos espacios e iniciativas de colaboración que buscan promover transformaciones hacia la sostenibilidad. Para hacer esta exploración, contaremos con una serie de especialistas que nos ayudarán a entender las causas y las consecuencias, así como la inclusión de formas alternativas de solución a distintos retos socioambientales. 

Acompáñanos en este episodio para examinar qué significa la innovación inclusiva, uno de los cuatro <a href="https://umbela.org/enfoques/">enfoques de trabajo</a> que Umbela busca incorporar en su quehacer. Hablaremos desde diferentes perspectivas sobre cómo se puede poner en práctica la innovación inclusiva utilizando como ejemplo el caso de la captación de agua de lluvia. La cosecha o captación de agua de lluvia ha surgido como una eco-tecnología y alternativa para aliviar los problemas de acceso a agua a los que se enfrentan millones de personas en el mundo. Es un esfuerzo que se ha impulsado en sitios como la Ciudad de México, particularmente en zonas donde habita poblaciones vulnerables y en condiciones de irregularidad o vivienda informal. En este episodio platicaremos de los retos a los que se enfrenta la instalación masiva de los sistemas de cosecha pluvial en México y cómo han contribuído o no estas iniciativas a la solución de la problemática socioambiental de escasez y acceso al agua.

En este episodio escucharemos cuatro voces distintas: personas que estudian el tema, quienes viven la escasez y beneificios de la captación, y uno de los emprededores enfocados en la cosecha de lluvia. Por la parte académica tenemos Beth Tellman, profesora de Geografía en la Universidad de Arizona y co-fundadora de Umbela, Beth realizó estudios sobre las distintas problematicas del agua en la Ciudad de México publicado en la revista Ecology and Society y otra investigación fue apoyada por Oxfam y se centró en analizar el potential de la captacion de agua de lluvia en la Ciudad de México. Jorge Ortiz Moreno terminó un doctorado en Estudios del Desarrollo por la Universidad de Sussex en Reino Unido. Parte de su tema de investigación es la masificación de captación de agua de lluvia en la Ciudad de México. También está presente Enrique Lomnitz, co-fundador de <a href="https://islaurbana.org/">Isla Urbana </a> - una organización clave para el diseño y la masificación de los sistemas de captación de agua de lluvia en México. Pero quizá la voz mas importante en este episodio es la de Bertha Soto Gonzáles, una beneficiaria del sistema de captación de agua de lluvia de la Alcaldía Tlalpan. Ella nos comparte su experiencia ante la escasez de agua en la Ciudad de México y las innovaciones para solventarla.


