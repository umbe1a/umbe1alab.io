---
date: 2021-04-16
title: Conversatorio 1
category: proyectos
title_image: media/Conv1.png
image: https://en.umbela.org/media/Conv1.png
carrusel: True
author: David Manuel-Navarrete, Almendra Cremaschi, Patricia Pérez-Belmont, Lakshmi Charli-Joseph, J. Mario Siqueiros-García, Hallie Eakin 
---

### Regenerando la tierra para nuevas raíces
### Blog + video  

Lo que verás en este primer video es una conversación a fuego lento en donde cuatro cultivadores de saberes de Colombia y México "escarban" las raíces de sus propios sistemas de conocimiento, y que a través de la experiencia y la reflexión, han transitado hacia formas más sustentables y plurales de ver y comprender el mundo. Para ello, estos cultivadores cuestionan las formas de conocimiento que le fueron impuestas y se ha abierto, no sin conflicto, a voces, ideas, elementos y sustancias nuevas, que no sólo nutrieron, sino transformaron sus formas de aprehender y cultivar(se). Sus propuestas y reflexiones incluyen:

- Cuestionar el "pensamiento cuadrado" y los métodos que producen ciertos resultados, pero dejan un sentimiento de vacío en las personas que buscan transformaciones hacia la sostenibilidad.
- Reconocer que nos podemos enfrentar a situaciones incómodas y frustrantes, con barreras que ponen a prueba nuestra tolerancia o que incluso pueden romper con nuestra inocencia pero que al final nos permiten liberarnos para tomar nuevas actitudes para aceptar lo diferente y diverso.
- Revalorizar métodos para conversar y tejer puentes con lo no-humano (e.j., con el clima,los organismos, la montaña).
- Celebrar las diferencias desde un suelo de empatía basado en los elementos que nos son comunes a todes.

¿Es posible transformar las formas en que percibimos y comprendemos lo que nos rodea? ¿Qué estructuras debemos romper? ¿Con quiénes podemos contar en ese camino? ¿Qué oportunidades podemos identificar para transformarnos? ¿Qué desafíos implica esta transformación? Son algunas de las preguntas que surgen de la conversación.

Escucha en este video sobre éstas y otras de sus reflexiones sobre cómo aprovechar las grietas en nuestro propio suelo, y cómo airearlo, descompactarlo y, en definitiva, regenerarlo para construir entre todes un suelo global apto para el policultivo de conocimientos y saberes.


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/541131729?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Regenerando el suelo para nuevas raíces"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


<p>
<em>  "Within and across the various strands in this particular conversation, ‘social’ and ‘natural’ implications unfold inseparably together. A resulting vision emerges of deeply entangled political ecologies: in which ambiguous relations and flowing processes matter more than rigid categories and fixed structures. That all this is achieved through the little rectangular frames of Zoom, is all the more to the authors’ credit. I can’t wait for the next instalment…"           Andy Stirling
</em>
</p>
