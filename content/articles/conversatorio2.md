---
date: 2021-06-16
title: Conversatorio 2
category: proyectos
title_image: media/Conv2.png
image: https://umbela.org/media/Conv2.png
carrusel: True
author: Fernanda Pérez Lombardini, J. Mario Siqueiros-García, Lakshmi Charli-Joseph, Patricia Pérez-Belmont, Hallie Eakin, Almendra Cremaschi, Tania Campos, Enrico Cresta, Mirna Inturias, Loni Hensler, David Manuel-Navarrete
english: https://en.umbela.org/blog/conversatory-2/
---

### La siembra de sueños compartidos
### Blog + video  

La siembra de sueños compartidos nace de la idea del policultivo, donde conversamos y vivimos en la pluralidad, en la diversidad y en el diálogo desde la diferencia. Y es que en el policultivo, no sólo las cosechas son más diversas y los suelos se vuelven más ricos, sino que en las diferencias nos reconocemos a nosotres mismes y se enriquece la comunalidad: yo soy contigo y tú conmigo, pero nadie deja de ser quien es.

En este segundo conversatorio hablamos sobre lo que puede ser, imaginar sin limitaciones y sin ataduras mentales, mirando hacia el vacío, hacia los futuros que podemos sembrar. Estos futuros son utopías que pueden guiar nuestras acciones, y personas como Enrico, Loni, Tania y Mirna han hecho de esta idea su misión y se han embarcado en la tarea de pasar de la utopía como sueño a la utopía como proceso, del ¿qué sería si…?, a llenarse las manos de tierra, a labrar esas utopías día con día, y a hacerlas florecer.

Y claro, ni los sueños ni las utopías ni la comunalidad son un proceso fácil; pueden haber piedras en el camino, los suelos pueden estar muy erosionados y las condiciones pueden no ser propicias para la germinación de las semillas. “Son locuras – dicen unas personas – ¡sin agroquímicos no harás producir esta tierra!” ¿Cuántas veces no hemos escuchado esto? Pero quienes se aventuran en estos cultivos han aprendido a vivir con las heladas, con las plagas, con la falta de agua y con el exceso de sol.

Sumar la compañía, combinar, articular distintos saberes y cuestionar nuestras creencias y valores nos permite encontrarnos con quienes nos ven como les Otres y viceversa. Transformar la tierra para hacer crecer en donde había quienes decían que nada crecería requiere cambios en nuestro interior y cambios con les otres para constituirnos como un nosotres. Estos cambios no son necesariamente cómodos, de hecho, si son profundos, si son verdaderas transformaciones, casi por naturaleza generan molestia. Sin embargo, pueden también tener el efecto de hacer crecer raíces, micorrizas que nos conectan sutilmente, puentes subterráneos que nos hacen uno, a la vez que nos permiten mantener las diferencias.

Además de los cambios que se generan individual y colectivamente, la siembra de sueños también requiere tejer tiempos y espacios distintos. Dejar a un lado el tiempo lineal e imperativo que nos ha sido impuesto para encontrarnos con otros espacios figurativos, sociales y físicos, puede conducirnos hacia la exploración de ideas de tiempo distintas, como el tiempo necesario para llevar y sembrar las semillas al campo, el tiempo vivido, el tiempo de la Tierra...

Este conversatorio está lleno de semillas de sueños por explorar y sueños germinados por degustar. Hay tanto que decir que lo ideal sería tener una charla alrededor del fuego, siguiendo el tiempo del campo, ese que no responde a la inmediatez a la que la cultura actual nos tiene condicionados...


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/572822589?h=b893641e64&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="La siembra de sue&amp;ntilde;os compartidos"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

