---
title: Equidad
subtitle: Equidad
slug: valores/equidad
no_banner: True
status: hidden
english: https://en.umbela.org/valores/Equity/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_equidad.svg" width="40%" height="100%"></object>
</center>

Fomentamos esquemas que aborden las asimetrías y que visibilicen las
narrativas y dinámicas de marginación para promover condiciones de
bienestar social, actuales y futuras.
