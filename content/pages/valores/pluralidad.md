---
title: Pluralidad
subtitle: Pluralidad
slug: valores/pluralidad
no_banner: True
status: hidden
english: https://en.umbela.org/valores/Plurality/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_pluralidad.svg" width="40%" height="100%"></object>
</center>

Reconocemos y celebramos la diversidad de contextos y maneras de
conocer y sentipensar el mundo, los derechos de todas las formas de
vida (de generaciones presentes y futuras) desde una concepción de
reivindicación social y equilibrio ecológico.
