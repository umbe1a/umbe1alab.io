---
title: Respeto universal
subtitle: Respeto universal
slug: valores/respeto-universal
no_banner: True
status: hidden
english: https://en.umbela.org/valores/Universal-respect/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_respeto_universal.svg" width="40%" height="100%"></object>
</center>

Vemos la Tierra como un sólo sistema conectado en el que todos los
seres vivos y sus interrelaciones tienen el mismo nivel de
importancia, por lo que merecen el mismo respeto y empatía.
