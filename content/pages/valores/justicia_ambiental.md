---
title: Justicia Ambiental
subtitle: Justicia Ambiental
title_image: /theme/images/bg_justicia_ambiental.jpg
slug: valores/justicia-ambiental
no_banner: True
status: hidden
english: https://en.umbela.org/valores/Environmental-justice/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_justicia_ambiental.svg" width="40%" height="100%"></object>
</center>

Consideramos que la dualidad naturaleza-humano en la época del
antropoceno ha generado desposesión de recursos, territorios y
saberes, por lo que buscamos fomentar espacios participativos y de
auto-gobernanza que generen condiciones de mayor justicia.
