---
title: Inicio
url: /
save_as: index.html
template: raiz
title_image: /theme/images/bg_inicio.jpg
fa_icon: fa-arrow-circle-o-right
subtitle: Tejiendo transformaciones hacia la sostenibilidad
status: hidden
english: https://en.umbela.org/
---

</br>

Para diseñar caminos hacia la sostenibilidad de la Tierra e intentar
atender los problemas socio-ambientales que en ella existen, en
particular aquellos en el Sur Global, no existe solución única, ni un
sólo enfoque, así como tampoco hay cabida para el singular
-disciplina, conocimiento o ser humano- … pero sí hay pluralidad que
en ocasiones ha llevado a contradicciones y asimetrías… es por ello
que creemos necesario mirar diferente, desde dentro hacia afuera y
viceversa, deteniéndose a entender que existen diferentes realidades y
formas de ver la misma Tierra, en esos otros espacios donde existen
individuos que no siempre están cerca y que pueden ser diferentes… y
quizá, después de la introspección, podemos buscar maneras de crear un
´nosotros´, un equipo, una comunidad, una red, para tejer en conjunto
caminos hacia una Tierra más sostenible.

----

