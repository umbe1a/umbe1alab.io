---
title: Enfoques de trabajo
subtitle: De-construir para construir en conjunto
slug: enfoques
title_image: /theme/images/bg_enfoques.jpg
fa_icon: fa-lightbulb-o
english: https://en.umbela.org/Approaches/
---

En Umbela consideramos que la estructura jerárquica del sistema
hegemónico actual no permite abordar de manera integral los problemas
críticos del mundo y tampoco reconoce la diversidad biológica y social
(de clases, género, culturas, etnias), ni el valor de los movimientos
de resistencia y regeneración que buscan proteger dicha
diversidad. Además, reconocemos que los procesos de colonización, de
extractivismo, racismo, sexismo y otros tipos de discriminación y
opresión han generado una inequidad histórica muy profunda. Ello ha
tenido consecuencias como la marginación y situaciones de desventaja
de voces de grupos a quienes se les ha negado el mismo poder que
tienen grupos dominantes (por ejemplo, aquellos asociados a visiones
de occidente, anglosajonas, del norte global, y/o machistas). Es así
como a lo largo de la historia, muchas narrativas e ideas han sido
extraídas de sus contextos locales para despolitizarlas y volverlas
mercadeables, por lo que las voces marginadas han sido silenciadas en
la construcción de saberes y conocimientos, tanto en ámbitos
científicos, como en los sistemas de gobernanza.

Por lo tanto, en Umbela reconocemos la necesidad de impulsar procesos
de cambio más profundos hacia estados más justos y sostenibles. A
estos tipos de cambios se les llaman transformaciones, y buscan
generar cambios de raíz en paradigmas, valores, comportamientos,
prácticas, o normas. Las transformaciones hacia la sostenibilidad
requieren transgredir las estructuras y narrativas dominantes que
mantienen la pobreza, la desigualdad y la degradación socio-ecológica,
y para ello es necesario visibilizar y desafiar explícitamente lo que
está normalizado (como las prácticas coloniales, el extractivismo, el
consumo excesivo, o la injusticia socioambiental). Para Umbela es
fundamental crear espacios de diálogo entre el Sur y el Norte Global,
espacios en donde se reconozca explícitamente que la producción e
intercambio de conocimientos y prácticas son necesarios desde todas
las esquinas y trincheras posibles, siempre respetando y reconociendo
sus orígenes, sus contextos particulares, sus autores, y su lucha.

----

<div class="row mb-12 white" style="padding:1em">

<div class="col-md-6">
<h2>Enfoques teórico-metodológicos</h2>
Con base en nuestros planteamientos y valores, en Umbela seguimos
cuatro enfoques principales de trabajo:

<ul>
<li>Innovación inclusiva</li>
<li>Aprendizaje transgresivo</li>
<li>Transdisciplina</li>
<li>Decolonialidad</li>
</ul>

Haz click en cada enfoque de la imagen para ver su descripción y referencias -->
</div>

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques.svg" width="70%" height="130%"></object>
</center>
</div>

</div>

<div class="row mb-12 white" style="padding:1em">

<div class="col-md-6">
<h2>El trabajo de Umbela</h2>
Umbela desarrolla y colabora en proyectos que contribuyan a la búsqueda de trayectorias hacia las transformaciones sostenibles, utilizando alguno o varios de nuestros enfoques teórico-metodológicos. A lo largo de nuestro trabajo profesional hemos colaborado con áreas del sector público, privado, organizaciones de la sociedad civil y academia priorizando esquemas de trabajo de investigación-acción-participativa en temas como: agencia colectiva; gobernanza colaborativa; fortalecimiento de capacidades; desarrollo y planeación territorial rural, periurbana y urbana; y estrategias relacionadas con las variaciones climáticas.
</div>

<div class="col-md-6">
<h2>Capacidades</h2>
<img alt="Capacidades Umbela" src="/media/capacidades.png" width="100%" />
</div>

</div>


### Tipo de productos

- Herramientas y guías para el trabajo transdisciplinario
- Espacios para procesos participativos
- Objetos de frontera para la comunicación entre diversos quehaceres y comunidades de práctica (mapas, juegos, etc.)
- Sistemas de monitoreo, reporte y verificación (MRV)
- Plataformas y modelos computacionales
- Análisis y desarrollo de capacidades en diferentes sectores
- Materiales didácticos y de difusión

<center>
<img src="/media/areas_1.jpg" width="23%" />
<img src="/media/areas_2.jpg" width="23%" />
<img src="/media/areas_3.jpg" width="23%" />
<img src="/media/areas_4.jpg" width="23%" />
</center>

----

### ¿Cómo podemos colaborar contigo o con tu organización?

Si tienes alguna idea o propuesta en la que consideres que podemos colaborar en conjunto, por favor escríbenos a [nuestro contacto](https://umbela.org/contacto/) para conversar sobre posibles formas de interactuar. Umbela es una asociación civil sin fines de lucro por lo que priorizamos esquemas horizontales de colaboración a modo de consorcios y/o alianzas estratégicas con otras organizaciones de la sociedad civil, instituciones académicas, sector privado, etc.
