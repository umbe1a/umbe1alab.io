---
title: Experimentos con cajitas
subtitle: Experimentos con cajitas
slug: experimentos
title_image: /theme/images/bg_proyectos.jpg
fa_icon: fa-folder-o
english: https://en.umbela.org/projects/
status: hidden
---

## Evento de lanzamiento Umbela Transformaciones Sostenibles





<!-- etiqueta más exterior, contiene las columnas -->
<div class="row mb-12 bg-light" style="padding:2em">

<div class="col-md-4">
Para dar inicio a este proyecto, y reconociendo las limitaciones actuales dadas por la situación global de la pandemia, Umbela iniciará su labor como asociación civil a través de un evento de lanzamiento virtual. Con este evento, co-diseñado con algunos de nuestros aliados estratégicos, se busca contribuir a la creación de puentes y a fortalecer alianzas entre individuos, colectivos, y organizaciones interesados en compartir y aprender sobre diversas lógicas y formas de actuar para impulsar procesos hacia la sostenibilidad de los sistemas socio-ecológicos.
</div>

<div class="col-md-2 bg-dark">
segunda columna
</div>

<div class="col-md-4">
Con este evento, co-diseñado con algunos de nuestros aliados estratégicos, se busca contribuir a la creación de puentes y a fortalecer alianzas entre individuos, colectivos, y organizaciones interesados en compartir y aprender sobre diversas lógicas y formas de actuar para impulsar procesos hacia la sostenibilidad de los sistemas socio-ecológicos.
</div>

<div class="col-md-2">
<img src="https://upload.wikimedia.org/wikipedia/commons/8/8e/Surfside_condominium_collapse_photo_from_Miami-Dade_Fire_Rescue_1.jpg" width="100%">
</div>

</div>
