---
title: Incidencia
subtitle: Incidencia
slug: proyectos/incidencia
no_banner: True
status: hidden
english: https://en.umbela.org/projects/advocacy/
---

<center>
<object type="image/svg+xml" data="/theme/images/proyectos.svg" width="40%" height="100%"></object>
</center>

## Incidencia

Actividades de  incidencia en las que hemos contribuido con algunos 
de los enfoques de trabajo de Umbela para fomentar narrativas transformadoras 
tanto en espacios de divulgación como en espacios de toma de decisión.

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Pacto Transformativo del agua</h3>
Conferencia de la ONU sobre el Agua 2023
<center>
<a href= "https://transformativewaterpact.org/">
<img alt="TWP" src="/media/twp.jpg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Así se ve el agua en México</h3>
Documentación fotográfica colectiva sobre la situación del agua en México
2022 - 2023
<center>
<a href= "https://susmai.sdi.unam.mx/index.php/en/actividades/documentacion-fotografica">
<img alt="AAM" src="/media/asiseveelagua.jpeg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
</div>

</div>

----
