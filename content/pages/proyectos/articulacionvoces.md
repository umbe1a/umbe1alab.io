---
title: Articulación de voces
subtitle: Articulación de voces
slug: proyectos/articulacionvoces
no_banner: True
status: hidden
english: https://en.umbela.org/projects/articulationvoices/
---

<center>
<object type="image/svg+xml" data="/theme/images/proyectos.svg" width="40%" height="100%"></object>
</center>

## Articulación de voces

Te invitamos a conocer algunos de los proyectos en los que hemos 
colaborado con el diseño y facilitación de procesos participativos 
con el fin crear espacios para el diálogo y la articulación de voces 
para fomentar trayectorias de transformación hacia la sostenibildidad. 

<h6><i>Lo sentimos, algunas ligas aún no están habilitadas...</i></h6>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Sistema de Acción Climática por los Mares y Costas de Yucatán</h3>
Talleres participativos para establecer estrategias para el cambio climático en la 
Península de Yucatán, 2023
<center>
<a href= "https://umbela.org/cultivando-saberes/">
<img alt="sacmyc2023" src="/media/proyectos/sacmyc2023.jpeg" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Cartografía participativa sobre futuros posibles</h3>
Elbaoración de una maqueta de futuros sostenibles para 
San Juan Tlacotenco, Morelos 2023
<center>
<img alt="cartografiapart" src="/media/cartografiapart.jpg" width="80%" />
</center>
</div>

<div class="col-md-4">
<h3>“Una Salud: de la prevención de enfermedades zoonóticas emergentes”</h3>
Diálogos para generar estrategias de prevención en la 
Península de Yucatán 2022
<center>
<img alt="onehealth" src="/media/proyectos/onehealth.jpeg" width="80%" />
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Intercambio de experiencias entre productores</h3>
Milpa Alta, Xochimilco y Tláhuac, CdMx 2022
<center>
<img alt="proyecto2" src="/media/encuentro.jpg" width="80%" />
</center>
</div>

<div class="col-md-4">
<h3>La ciudad costera que soñamos</h3>
Jornadas participativas con juventudes <br/>
Los Cabos, Boca del Río, Bacalar, Chetumal - 2022
<center>
<a href= "https://www.youtube.com/watch?v=BgRbDd17YIQ">
<img alt="taller1" src="/media/bacalar.JPG" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Red Transdisciplinaria para la Producción Pecuaria Sostenible</h3>
Yucatán 2021 - 2022
<center>
<img alt="taller2" src="/media/yucatan.jpeg" width="80%" />
</center>
</div>

</div>

----
