---
title: Mediateca
subtitle: Mediateca
slug: proyectos/mediateca
no_banner: True
status: hidden
english: https://en.umbela.org/projects/medialibrary/
---

<center>
<object type="image/svg+xml" data="/theme/images/proyectos.svg" width="40%" height="100%"></object>
</center>

## Mediateca

Consulta una diversidad de contenido digital 
(podcast, poemas, lecturas rápidas, libritos y más).

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Cosecha de agua de lluvia en CDMX- una innovación inclusiva?</h3>
Podcast 
<center>
<a href= "https://umbela.org/blog/cosecha-de-agua-de-lluvia-en-cdmx-una-innovacion-inclusiva/">
<img alt="connections" src="/media/capturadelluvia_foto.png" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>y por eso te necesito</h3>
Poema de Susi Moser
<center>
<a href= "https://umbela.org/blog/and-this-is-why-i-need-you/">
<img alt="connections" src="/media/connections.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>El viaje de Ronin</h3>
Librito - Aprendizaje transgresivo
<center>
<a href= "https://umbela.org/blog/el-aprendizaje-transgresivo-como-un-catalizador-de-cambio-profundo">
<img alt="AT" src="/media/portada_tinyb.png" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Regenerando la tierra para nuevas raíces</h3>
Video + blog - Proyecto "Cultivando saberes"
<center>
<a href= "https://umbela.org/blog/conversatorio-1/">
<img alt="Conv1" src="/media/Conv1.png" width="80%" />
</a>
</center>
</div>

</div>


<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>La siembra de sueños compartidos</h3>
Video + blog - Proyecto "Cultivando saberes"
<center>
<a href= "https://umbela.org/blog/conversatorio-2/">
<img alt="Conv2" src="/media/Conv2.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Cosechando saberes</h3>
Video + blog - Proyecto "Cultivando saberes"
<center>
<a href= "https://umbela.org/blog/conversatorio-3/">
<img alt="Conv3" src="/media/Conv3.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Cuidando y nutriendo la tierra</h3>
Video + blog - Proyecto "Cultivando saberes"
<center>
<a href= "https://umbela.org/blog/conversatorio-4/">
<img alt="Conv4" src="/media/Conv4.png" width="80%" />
</a>
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Waves of challenging research for sustainability</h3>
Blog - Methods series
<center>
<a href= "https://steps-centre.org/blog/waves-of-challenging-research-for-sustainability/">
<img alt="waves" src="/media/waves-of.jpg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Transdisciplinary methods, relationships, politics and praxis</h3>
Blog - Methods series
<center>
<a href= "https://steps-centre.org/event/challenging-research-for-sustainability-transdisciplinary-methods-relationships-politics-and-praxis/">
<img alt="CR" src="/media/challenging-research.jpg" width="80%" />
</a>
</center>
</div>


<div class="col-md-4">
<h3>The ‘methods bazar’ </h3>
Blog - What did we learn about transdisciplinary methods for sustainability?
<center>
<a href= "https://steps-centre.org/blog/the-methods-bazaar-what-did-we-learn-about-transdisciplinary-methods-for-sustainability/">
<img alt="methodsbazaar" src="/media/methodsbazaar.png" width="80%" />
</a>
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Lanzamiento de Umbela</h3>
<a href="https://youtu.be/Opk4FNe-_xI">Evento virtual</a> - Hablamos de nuestras motivaciones, inspiraciones y experiencias 
al poner en práctica nuestros enfoques
</i>
<center>
<a href= "https://umbela.org/blog/lanzamiento-del-proyecto-umbela">
<img alt="lanzamiento" src="/media/lanzamiento.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
</div>

<div class="col-md-4">
</div>

</div>
----
