---
title: Investigación
subtitle: Investigación
slug: proyectos/investigación
no_banner: True
status: hidden
english: https://en.umbela.org/projects/research/
---

<center>
<object type="image/svg+xml" data="/theme/images/proyectos.svg" width="40%" height="100%"></object>
</center>

## Investigación

Participación en proyectos de corte académico.

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Trayectorias de transformación sobre el agua</h3>
Humedal de Xochimilco 2023 - 2027
<center>
<a href="https://trans-path-plan.com/">
<img alt="wtpp" src="/media/proyectos/wtpp.png" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Espejos de agua</h3>
Humedal de Xochimilco 2023 - 2027
<center>
<a href="https://sites.google.com/view/water-mirrors-project/home">
<img alt="watermirrors" src="/media/proyectos/watermirrors.png" width="100%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Cultivando saberes</h3>
Serie de videos, blogs, audio-poemas e imágenes para explorar cómo 
cultivar saberes para fomentar transformaciones. 
<center>
<a href= "https://umbela.org/cultivando-saberes/">
<img alt="CS" src="/media/conversatorios_fotos.png" width="80%" />
</a>
</center>
</div>

</div>

----
