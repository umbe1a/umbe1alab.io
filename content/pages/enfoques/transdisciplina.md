---
title: Transdisciplina
subtitle: Transdisciplina
slug: enfoques/transdisciplina
no_banner: True
status: hidden
english: https://en.umbela.org/enfoques/Transdisciplinarity/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_transdisciplina.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Transdisciplina</h2>
<p>Se entiende como transdisciplina al proceso relacional e integrador de diversos tipos de conocimiento y perspectivas donde se remarca la importancia de los saberes no académicos ni disciplinarios. Se acciona mediante procesos de diálogo, colaboración y constante interacción entre la teoría-práctica, valores e intereses. Este esfuerzo de colaboración se enfoca en el abordaje de problemas socioambientales y encuentra en la transdisciplina un marco reflexivo que hace posible integrar saberes provenientes de diferentes sectores y cuerpos de conocimiento.  La transdisciplina permite el aprendizaje conjunto, nuevas relaciones de cooperación y auto-organización, y busca llevar el conocimiento a la acción para fomentar transformaciones hacia la sostenibilidad de los sistemas socioambientales que fomenten cambios profundos y duraderos con una visión de justicia.
</p>

<p><i>Para conocer más sobre este enfoque, te recomendamos las siguientes lecturas:</i></p>
<ul>
<li>Juliana Merçon, Bárbara Ayala-Orozco y Julieta A. Rosell. 2018. <a href="https://drive.google.com/file/d/1LTDusAxv2bJHwrirBE3HmKFmKzeG405i/view?usp=sharing">Experiencias de colaboración transdisciplinaria para la sustentabilidad.</a></li>
<li>Russell A.W., et al. 2008. <a href="https://drive.google.com/file/d/1LSLaI9OEIxxKVJE2kY_EAxhLZs_GXaaY/view?usp=sharing">Transdisciplinarity: Context, contradictions and capacity.</a></li>
<li>Lang D.J., et al. 2012. <a href="https://drive.google.com/file/d/1LSLJQOxc5sVLpaHUULqdnvl8_BWZuiSk/view?usp=sharing">Transdisciplinary research in sustainability science: practice, principles, and challenges.</a></li>
</ul>

</div>

</div>

