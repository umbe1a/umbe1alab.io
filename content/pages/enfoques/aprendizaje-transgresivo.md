---
title: Aprendizaje Transgresivo
subtitle: Aprendizaje Transgresivo
slug: enfoques/aprendizaje-transgresivo
no_banner: True
status: hidden
english: https://en.umbela.org/enfoques/Transgressive-learning/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_aprendizaje-transgresivo.svg" width="100%" height="200%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Aprendizaje transgresivo</h2>
<p>El aprendizaje transgresivo es una forma de aprendizaje colectivo orientado hacia la acción, y que busca el empoderamiento a través de maneras más profundas y reflexivas de conocer. Este enfoque implica plantear formas más radicales de cuestionar y representar lo que podrían significar las transformaciones hacia la sostenibilidad en diversos contextos, y desafía lo que se ha normalizado (i.e., business as usual, status quo). El aprendizaje transgresivo se centra en generar un pensamiento crítico a través de esquemas que intencionalmente promuevan cambios en prácticas dominantes e injustas, y en activar la agencia colectiva. Dichos esquemas se diseñan para transgredir y explorar cómo otros mundos son posibles desde la afectividad, el cuidado, y la responsabilidad ética.
</p>

<p><i>Para conocer más sobre este enfoque, te recomendamos las siguientes lecturas:</i></p>
<ul>
<li>A. Cremaschi, L. Charli-Joseph, F.I. Ciocchini. 2022. <a href="https://umbela.org/blog/el-aprendizaje-transgresivo-como-un-catalizador-de-cambio-profundo/">Blog sobre aprendizaje transgresivo y Librito "El viaje de Ronin, una experiencia de aprendizaje transgresivo"</a> 

<li>Red de aprendizaje transgresivo <a href="https://transgressivelearning.org/">(T-Learning)</a></li>
<li>T. Macintyre, M. Chaves, D. McGarry. 2018. <a href="https://drive.google.com/file/d/1wXDvsiHDO9GLJ19zx4WjdnbNt8VvcvU7/view?usp=sharing">Marco conceptual del espiral vivo.</a></li>
<li>Lotz-Sisitka et al. 2015. <a href="https://drive.google.com/file/d/1LQ88zWieZw2AE2MnFR781h5JVCkcstkP/view?usp=sharing">Transformative, transgressive social learning: rethinking higher education pedagogy in times of systemic global dysfunction.</a></li>
</ul>

</div>

</div>

