---
title: Innovación Disruptiva
subtitle: Innovación Disruptiva
slug: enfoques/innovacion-disruptiva
no_banner: True
status: hidden
english: https://en.umbela.org/enfoques/Disruptive-innovation/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_innovacion-disruptiva.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Innovación inclusiva</h2>
<p>La innovación inclusiva comprende iniciativas como productos, procesos, programas, proyectos o plataformas que buscan cuestionar las percepciones y creencias sobre rutinas, recursos y relaciones de poder que definen la distribución de un bien o servicio de forma desigual, injusta e inequitativa (transgresiva). Las innovaciones inclusivas pueden ser soluciones simples, accesibles y de bajo costo dirigidas a incrementar el acceso de poblaciones no privilegiadas, marginadas y tradicionalmente excluidas a oportunidades y a capital socioeconómico (frugales). Además, las innovaciones inclusivas pueden ocurrir en diversas arenas comunitarias y de la sociedad civil, involucrando a agentes de cambio que experimentan con innovaciones sociales para cubrir las necesidades socioambientales de la comunidad, incluyendo la diversidad de saberes y conocimientos alternativos a formas convencionales de entender el mundo (de base o comunitarias). En este caso, dichas innovaciones pueden ser creadas en colectivo (en comunidades de práctica, indígenas, o movimientos locales) y con recursos y materiales existentes. Un ejemplo sería la captación de agua de lluvia, para no tener que pagar ni depender del Estado ni las políticas para recibir el agua en el hogar y, con ello, crear opciones que las mismas comunidades usuarias pueden implementar. Así, las innovaciones sociales pueden contribuir a una economía social, como parte de actividades comunitarias y de emprendedurismo social. Finalmente, las innovaciones inclusivas pueden contribuir a detonar cambios transformativos de un sistema que afectan múltiples escalas y que afecta a más de un grupo, organizaciones y comunidades específicas y, con ello, aspirando a incidir en todo el sistema (p.ej. cambios culturales, legales, económicos, etc.).
</p>

</div>

</div>
