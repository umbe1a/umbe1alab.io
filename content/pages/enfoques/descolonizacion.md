---
title: Descolonización
subtitle: Descolonización
slug: enfoques/descolonizacion
no_banner: True
status: hidden
english: https://en.umbela.org/enfoques/Decolonising/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_descolonizacion.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Decolonialidad</h2>
<p>El enfoque de decolonialidad implica visibilizar explícitamente las estructuras y formas de pensamiento que han persistido aún después de la independencia de las naciones (descolonización). La decolonialidad nos invita a aceptar la diversidad de saberes, de formas de pensar y conocer el mundo, a reconocer a las diferentes culturas y a luchar contra la opresión,  los sistemas de dominación y discriminación que han aparecido bajo diferentes configuraciones históricas en diferentes territorios. Este enfoque permite poner en el centro del análisis la dimensión política asociada a situaciones de injusticia y marginación. Para ello, es crucial fomentar esquemas de reflexividad colectiva dirigidos a retar las causas profundas de los problemas de sostenibilidad, con el fin de impulsar movimientos y estructuras más horizontales e incluyentes que permitan accionar hacia un bien común.</p>

<p><i>Para conocer más sobre este enfoque, te recomendamos las siguientes lecturas:</i></p>
<ul>
<li>Eduardo Restrepo, Axel Rojas. 2010. <a href="https://drive.google.com/file/d/1i_XExvCz4XWmpcCtBgGzIYziaAvhs3Ix/view?usp=sharing">Inflexión decolonial: fuentes, conceptos y cuestionamientos.</a> 
</li>
<li>Adela Parra-Romero. 2016. <a href=https://www.ecologiapolitica.info/?p=6006>¿Por qué pensar un giro decolonial en el análisis de los conflictos socioambientales en América Latina?</a>
</li>
</ul>

</div>

</div>
