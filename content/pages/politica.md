---
title: Términos de uso y política de privacidad
slug: politica
no_banner: True
status: hidden
english: https://en.umbela.org/politicas/
---

### Umbela Transformaciones Sostenibles, A.C.

### Términos y condiciones de uso

Esta página web umbela.org es propiedad y está operada por Umbela Transformaciones Sostenibles A.C. Estos “Términos y condiciones de uso” establecen los términos y condiciones bajo los cuales puede usar nuestra página web y servicios ofrecidos por nosotros. Esta página web ofrece a los visitantes la descripción de nuestra misión y visión, nuestros enfoques de trabajo, la red que conforma a “Umbela Transformaciones Sostenibles, A. C.” (fundadores, socios, asesores, colaboradores y aliados), así como los proyectos que llevamos a cabo y la forma en cómo puede vincularse con nosotros utilizando la pestaña de contacto. Al acceder o usar la página web de nuestro servicio, usted aprueba que haya leído, entendido y aceptado estar sujeto a estos Términos:

- Posesión de propiedad intelectual, derechos de autor y logos

El Servicio y todos los materiales incluidos o transferidos, incluyendo sin limitación, software, imágenes, texto, gráficos, logotipos, patentes, marcas registradas, marcas de servicio, derechos de autor, fotografías, audio, videos, música y todos los Derechos de Propiedad Intelectual relacionados con ellos, son la propiedad exclusiva de “Umbela Transformaciones Sostenibles, A. C.” Salvo que se indique explícitamente en este documento o en la sección de la página correspondiente, no se considerará que nada en estos Términos crea una licencia bajo ninguno de dichos Derechos de Propiedad Intelectual. Usted acepta no vender, licenciar, alquilar, modificar, distribuir, copiar, reproducir, transmitir, exhibir públicamente, publicar, adaptar, editar o crear trabajos derivados de los mismos.

- Limitación de responsabilidad

En la máxima medida permitida por la ley aplicable, en ningún caso “Umbela Transformaciones Sostenibles, A. C.” será responsable por daños indirectos, punitivos, incidentales, especiales, consecuentes o ejemplares, incluidos, entre otros, daños por pérdida de beneficios, buena voluntad, uso, datos u otras pérdidas intangibles, que surjan de o estén relacionadas con el uso o la imposibilidad de utilizar nuestros servicios y contenido. 

En la máxima medida permitida por la ley aplicable, “Umbela Transformaciones Sostenibles, A. C.” no asume responsabilidad alguna por (i) errores o inexactitudes de contenido; (ii) lesiones personales o daños a la propiedad, de cualquier naturaleza que sean, como resultado de su acceso o uso de nuestro servicio y contenido; y (iii) cualquier acceso no autorizado o uso de nuestros servidores seguros y/o toda la información personal almacenada en los mismos.

- Derecho a cambiar y modificar los Términos

Nos reservamos el derecho de modificar estos términos periódicamente a nuestra entera discreción. Cuando cambiemos los Términos de una manera material, le notificaremos que se han realizado cambios importantes en los Términos. El uso de la página web o nuestro servicio después de dicho cambio constituye su aceptación de los nuevos Términos. Si no acepta alguno de estos términos o cualquier versión futura de los Términos, no use o continúe accediendo a la página web o a nuestros servicios.

- Correos electrónicos de promociones y contenido

Acepta recibir nuestros mensajes y materiales de difusión por correo electrónico o cualquier otro formulario de contacto en el que nos proporcione sus datos personales (incluido su número de teléfono para llamadas o mensajes de texto). Si no desea recibir dichos materiales o avisos de promociones puede notificarnos al correo contacto@umbela.org.

### Aviso de privacidad

En cumplimiento con la Ley Federal de Protección de Datos Personales en Posesión de Particulares (la “Ley”), y demás relacionadas, nos permitimos informarle sobre las características y finalidades del tratamiento que se dará a sus datos personales que por el ejercicio de sus actividades realice Umbela Transformaciones Sostenibles A. C. (Umbela).

- Identidad y domicilio del responsable

Umbela es una Asociación Civil que tiene por objeto  impulsar la transformación del mundo hacia la sostenibilidad, a través de proyectos que generen espacios de colaboración para articular la diversidad de saberes, culturas, enfoques y conocimientos que contribuyan a desarrollar e incrementar condiciones de bienestar, justicia y equidad en la sociedad y en el ambiente. 

Umbela se encuentra ubicada en la Ciudad de México, México y es responsable de recabar sus datos personales, del uso y almacenamiento que se le dé a los mismos y de su debida protección.

- Datos recabados

Respecto a la información de sus datos personales, ésta podrá ser proporcionada personal y directamente por usted, ya sea de manera verbal o por escrito, por correo electrónico o correspondencia, tarjetas de presentación, o por llenado de formularios de contactos y de datos personales recabada a través de nuestro sitio de Internet, o cualquier otro medio.

Recabamos de usted los siguientes datos personales:
Nombre y apellido
Dirección de correo electrónico
Número de teléfono

- ¿Para qué utilizamos sus datos personales?

Para verificar la identidad y responder a sus solicitudes entre usted y Umbela.
Para convenir nuestros servicios.
Para el seguimiento de los servicios adquiridos o contratos concretados.
Enviarle información sobre nuestros eventos, convocatorias, y otras iniciativas relacionadas con Umbela.
Analizar información para efectos de estadísticas sobre el impacto de nuestros proyectos.

En caso de que no desee que sus datos personales se utilicen para estos fines, indíquelo enviándonos un correo a contacto@umbela.org.
La negativa para el uso de sus datos personales para estas finalidades no podrá ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.

- ¿Dónde puedo consultar el aviso de privacidad integral?

Para conocer mayor información sobre los términos y condiciones en que serán tratados sus datos personales, como los terceros con quienes compartimos su información personal y la forma en que podrá ejercer sus derechos ARCO, puede consultar el aviso de privacidad integral en: umbela.org o podrá solicitarlo enviándonos un correo a contacto@umbela.org.
Información adicional

I. Es de nuestro especial interés cuidar la información personal sensible, aquella que revele aspectos como origen racial, étnico, estado de salud, información genética, información de género, creencias religiosas, filosóficas y morales, afiliación sindical, opiniones políticas, preferencias sexuales, sobre menores de edad, personas con discapacidades y las personas que han sido declaradas en interdicción. Estos datos serán tratados únicamente para las finalidades descritas en este Aviso de Privacidad.

II. Los datos personales que se recaben formarán parte de una base de datos que permanecerá vigente durante el tiempo que exista Umbela, o durante el periodo necesario para cumplir la finalidad mencionada, por lo que adoptaremos todas y cada una de las medidas de seguridad administrativa, física y técnica, establecidas en la Ley, necesarias para salvaguardar su información personal de cualquier daño, pérdida, alteración, destrucción o del uso, acceso, o tratamiento no autorizado.

III. Si usted considera que su derecho a la protección de sus datos personales ha sido lesionado por alguna conducta u omisión de nuestra parte, o presume alguna violación a las disposiciones previstas en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y demás ordenamientos aplicables, podrá interponer su inconformidad o denuncia ante el Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI). Para más información, le sugerimos visitar su página oficial de Internet www.inai.org.mx.

IV. Al proporcionarnos sus datos otorga su consentimiento para que sus datos personales sean tratados conforme a lo señalado en el presente aviso de privacidad o si lo desea, puede hacerlo de manera expresa enviando correo electrónico a: contacto@umbela.org, con el asunto: “Consentimiento”; añadiendo en el mensaje la siguiente leyenda: “Otorgo mi consentimiento para que mis datos personales sean tratados conforme a lo señalado en el presente aviso de privacidad”.

- Modificaciones al aviso de privacidad

El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de trabajo, o por otras causas.

Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad a través de nuestra página: umbela.org.

Fecha de actualización: abril 2021.

