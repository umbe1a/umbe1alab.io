---
title: Acerca de Umbela
subtitle: Cuestionar, explorar y crear nuevas formas de entender el mundo
slug: acerca
title_image: /theme/images/bg_acerca.jpg
fa_icon: fa-bookmark-o
english: https://en.umbela.org/about-us/
---


Umbela es una Asociación Civil que tiene por objeto impulsar la
transformación del mundo hacia la sostenibilidad, a través de procesos
innovadores que generen espacios de colaboración para articular la
diversidad de saberes, culturas, enfoques y conocimientos que
contribuyan a desarrollar e incrementar condiciones de bienestar,
justicia y equidad en la sociedad y en el ambiente.

<div style="width: 28rem; margin: 0 0 1em 3em;" class="float-md-right">
<h2>Visión</h2>
<p style="font-weight: 100;">
Conducir las ciencias de la sostenibilidad para
fomentar procesos de transformación hacia trayectorias justas,
incluyentes y equitativas en la sociedad y el ambiente.
</p>

<h2>Misión</h2>
<p style="font-weight: 100;">
Impulsar transformaciones hacia la sostenibilidad en
sistemas socioambientales a través de fortalecer capacidades,
articular la diversidad de saberes, facilitar espacios y procesos
participativos de colaboración, e implementar soluciones innovadoras.
</p>
</div>


Nuestro mayor interés es cuestionar, explorar y crear nuevas formas de
entender el mundo para buscar soluciones a los problemas socio
ambientales que actualmente ponen en riesgo la sostenibilidad de la
Tierra. Sabemos que existe una voluntad colectiva que cree que un
cambio es posible, y es por ello que queremos contribuir a generar ese
cambio buscando nuevos significados y entendimientos, de-construyendo
y reconstruyendo a través de nuestros proyectos que buscan cumplir con
los siguientes objetivos:

 - Fortalecer las capacidades de diferentes actores de la sociedad mediante herramientas de colaboración adaptativas, incluyentes y plurales.
 - Dar voz a los grupos y voces marginadas que se encuentran en situaciones de conflictos socioambientales.
 - Buscar soluciones innovadoras y colaborativas con el fin de contribuir al bienestar social y a la integridad ambiental global.
 - Promover procesos transdisciplinarios que trascienden las disciplinas, instituciones, y jerarquías para co-construir visiones y decisiones que detonen caminos de transformación hacia la sostenibilidad.
 - Construir alianzas, y espacios de diálogo y colaboración a través de esquemas de involucramiento continuo.
 
----

### El trabajo de Umbela se inspira y guía desde cuatro valores fundamentales:

(Haz click en cada valor para ver su descripción)

<center>
<object type="image/svg+xml" data="/theme/images/valores.svg" width="40%" height="100%"></object>
</center>


