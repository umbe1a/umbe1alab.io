---
title: STEPS Centre
role: Prof. Andy Stirling,  Dra. Marina Apgar
slug: equipo/steps_centre
headshot: /media/logo_steps.png
status: hidden
template: perfil
english: https://en.umbela.org/allies/steps_centre/
---

<br />

Dr. Andrew Stirling

Investigador - Unidad de Investigación en Ciencias Políticas y
Co-director del Centro STEPS, Universidad de Sussex

_Investigación transdisciplinaria, asesoría de políticas públicas sobre
la política de la ciencia, la tecnología y la innovación;
Incertidumbre, participación, diversidad y sostenibilidad en la
gobernanza de la ciencia y la innovación_


<https://profiles.sussex.ac.uk/p7513-andrew-stirling>


----
Dra. Marina Apgar

Investigadora - Cluster de Partipación, Inclusión y Cambio Social, Instituto de Estudios del Desarrollo;
Académica del Centro STEPS, Universidad de Sussex

_Investigación Acción Participativa; Metodologías de planeación, monitoreo y evaluación de procesos participativos y transdisciplinarios_

<https://www.ids.ac.uk/people/marina-apgar/>
