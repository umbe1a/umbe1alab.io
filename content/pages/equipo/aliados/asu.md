---
title: ASU 
role: Prof. Hallie Eakin,     Dr. David Manuel-Navarrete
slug: equipo/asu
headshot: /media/imagen_temporal_asu.png
status: hidden
template: perfil
english: https://en.umbela.org/equipo/asu/
---

<br />

Prof. Hallie Eakin

Investigadora – Escuela de Sostenibilidad, Colegio de Futuros Globales, Universidad Estatal de Arizona (Arizona State University -ASU)

_Adaptación al cambio climático, vulnerabilidad y resiliencia, transformaciones hacia la sostenibilidad_

<https://sustainability-innovation.asu.edu/person/hallie-eakin/>

-----

Dr. David Manuel-Navarrete

Investigador – Escuela de Sostenibilidad, Colegio de Futuros Globales, Universidad Estatal de Arizona (Arizona State University -ASU)

_Gobernanza del cambio climático, integridad ecológica, inequidad socio-ecológica, sistemas de conocimiento 
para la agricultura sostenible_

<https://sustainability.asu.edu/person/david-manuel-navarrete/>
