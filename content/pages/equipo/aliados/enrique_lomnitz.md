---
title: Isla Urbana
role: Enrique Lomnitz, Director General
slug: equipo/isla_urbana
headshot: /media/logo-isla-urbana.png
status: hidden
template: perfil
english: https://en.umbela.org/allies/isla_urbana/
---

<br />

Director General de [Isla Urbana](https://islaurbana.org/)


_Construcción de infraestructura descentralizada para el acceso
sostenible al agua, especialmente para las áreas más marginadas de
México; Desarrollo regenerativo y local; Sistemas de captación de agua
de lluvia_


<https://islaurbana.org/>

<br />

