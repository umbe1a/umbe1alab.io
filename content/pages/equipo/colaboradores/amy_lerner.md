---
title: Dra. Amy M. Lerner
role: Colaboradora
slug: equipo/amy-lerner
headshot: /media/amy-lerner.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/amy-lerner/
---

_Procesos de cambio de uso de suelo, especialmente en la frontera
rural-urbana; planeación urbana sostenible, resiliencia,
infraestructura verde; agricultura urbana y periurbana, así como la
interfaz ciencia-política pública_

Amy cuenta con un doctorado en Geografía de la Universidad de
California, Santa Bárbara (2011). Posteriormente fue investigadora
asociada postdoctoral en la Universidad de Rutgers, Nueva Jersey, en
los departamentos de Geografía y Ecología Humana (2011-2013) y en la
Escuela de Asuntos Públicos e Internacionales de la Universidad de
Princeton (2013-2015). 

De 2015 a 2020, la Dra. Lerner fue investigadora en el Laboratorio
Nacional de Ciencias de la Sostenibilidad en el Instituto de Ecología
de la Universidad Nacional Autónoma de México, UNAM, donde dirigió
varios proyectos de investigación relacionados con las asociaciones
entre gobierno de la Ciudad de México y la UNAM, la persistencia de la
agricultura periurbana y el desarrollo de capacidades en el gobierno
local de la ciudad para la gestión de riesgos y la resiliencia
urbana.

Cuenta con una amplia experiencia docente en cursos sobre
ciencias de la sostenibilidad, métodos para la investigación
transdisciplinaria y cambio global. Actualmente es profesora asociada en la universidad UC San Diego.

<https://usp.ucsd.edu/people/faculty/profiles/lerner_amy.html>
