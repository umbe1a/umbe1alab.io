---
title: Dra. Beatriz López
role: Colaboradora
slug: equipo/beatriz-lopez
headshot: /media/Beatriz_Lopez.jpeg
status: hidden
template: perfil
english: https://en.umbela.org/equipo/beatriz-lopez/
---

Beatriz tiene facilidad para trabajar con grupos interdisciplinarios en distintos escenarios ecosistémicos como áreas de conservación forestal, ecosistemas oceánicos (Pacífico colombiano) humedales, bosques andinos y zonas de frontera entre países como es la Triple Frontera Amazónica Brasil-Colombia y Perú. Ha participado en el desarrollo de proyectos a nivel comunitario y en distintas escalas geográficas, con diversidad de actores organizacionales e institucionales bajo esquemas sustentables. En este contexto también ha formulado y ejecutado de manera concertada, líneas de acción en torno a la transición agroecológica en comunidades indígenas, campesinas y afrodescendientes. También he tenido la gran oportunidad de aplicar sus conocimientos en la caracterización de conflictos socioambientales ocasionados por situaciones de conflicto armado en Colombia y por la existencia de megaproyectos. A Beatriz, le entusiasma la planificación y ejecución de eventos comunitarios y académicos, así como la aplicación de herramientas participativas y trabajar de manera conjunta con aportes artísticos como el diseño gráfico, fotografías y cortometrajes que contribuyan a la circulación del conocimiento y de la información.
