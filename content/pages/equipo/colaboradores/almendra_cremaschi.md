---
title: Dra.(c) Almendra Cremaschi
role: Colaboradora
slug: equipo/almendra-cremaschi
headshot: /media/almendra-cremaschi.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/almendra-cremaschi/
---

_Agronomía sustentable, procesos participativos, inteligencia colectiva, innovación colaborativa_

Ingeniera Agrónoma de la Universidad Nacional de La Plata, Argentina y candidata a doctora por la misma Universidad (FCAyF-UNLP) con la investigación titulada “Emergencia de nichos a partir de innovaciones de base en el sistema de semillas de Argentina”, gracias a una beca doctoral otorgada por el Consejo Nacional de Investigaciones Científicas y Técnicas (CONICET). 
Actualmente, tiene lugar de trabajo en el [Centro de Investigación para la Transformación](http://fund-cenit.org.ar/) (CENIT), de la Universidad Nacional de San Martín, donde es profesora adjunta de Desarrollo Sustentable. 
Desde 2018 es co-fundadora de [Bioleft](https://www.bioleft.org/es/), un laboratorio comunitario que busca soluciones a los problemas de sustentabilidad de las semillas, a partir del uso de inteligencia colectiva y conocimiento abierto. En el marco de esta iniciativa, ha colaborado estrechamente con la Universidad Nacional Autónoma de México. 

Desde hace más de diez años trabaja en temas de sustentabilidad, agricultura familiar, y metodologías participativas de co-producción de conocimientos e innovaciones, tanto desde la extensión rural como de investigación en temas afines. En particular, se interesa por los procesos de coproducción de conocimientos en el marco de iniciativas transformadoras y por reflexionar y diseñar estrategias metodológicas que contribuyan a construir espacios justos y abiertos de creatividad e innovación colaborativa. 

