---
title: M. en C. Rodrigo García Herrera
role: Colaborador
slug: equipo/rodrigo-garcia
headshot: /media/Rodrigo_Garcia.jpg
status: hidden
template: perfil
english: https://en.umbela.org/equipo/rodrigo-garcia/
---

_Auto-organización, redes complejas, infraestructura de cómputo_

Rodrigo es ingeniero en Cibernética y Maestro en Ciencias de la Complejidad. Se interesa por el cambio socio-cultural profundo, a través del desarrollo de plataformas de organización horizontal autogestiva. 

Desarrolló este sitio web usando [Gitlab](https://docs.gitlab.com/ee/user/project/pages/) y [Pelican](https://docs.getpelican.com/en/stable/quickstart.html).

### Perfiles en Internet
- [ResearchGate](https://www.researchgate.net/profile/Rodrigo_Garcia-Herrera)
- [ORCiD](http://orcid.org/0000-0002-7972-5746)
- [Google Scholar](https://scholar.google.com.mx/citations?user=aLFvcZQAAAAJ)

### Repositorios de Software
- [GitLab](https://gitlab.com/rgarcia-herrera)
- [GitHub](https://github.com/rgarcia-herrera)
