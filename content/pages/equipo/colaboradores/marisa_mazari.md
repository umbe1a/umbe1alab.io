---
title: Dra. Marisa Mazari Hiriart
role: Colaboradora
slug: equipo/marisa-mazari
headshot: /media/Marisa_Mazari.jpeg
status: hidden
template: perfil
english: https://en.umbela.org/equipo/marisa-mazari/
---

_Usos sostenibles y monitoreo del agua_

Marisa es Bióloga por la UNAM, con maestría en Hidrobiología Aplicada por la Universidad de Gales, Gran Bretaña y Doctorado en Ciencias Ambientales e Ingeniería por UCLA, E.U. Es Investigadora Titular del Laboratorio Nacional de Ciencias de la Sostenibilidad del Instituto de Ecología y Coordinadora del Seminario Universitario de Sociedad, Medio Ambiente e Instituciones (SUSMAI) de la UNAM. Ha trabajado en grupos interdisciplinarios sobre usos sostenibles y monitoreo del agua en socio-ecosistemas tanto urbanos como rurales en México. Cuenta con más de 100 publicaciones y ha formado recursos humanos de nivel licenciatura y posgrado. Es Investigadora Nacional Nivel III del SNI.

- [LANCIS-IE-UNAM](https://lancis.ecologia.unam.mx/personal/marisa_mazari/)
- [SUSMAI-UNAM](https://susmai.sdi.unam.mx/index.php/en/)