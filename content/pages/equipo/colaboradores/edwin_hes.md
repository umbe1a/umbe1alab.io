---
title: Dr. Edwin Hes
role: Colaborador
slug: equipo/edwin-hes
headshot: /media/edwin-hes.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/edwin_hes/
---

_Investigación, creación de capacidades y facilitación de procesos en
el uso sostenible de humedales y modelación de flujos de nutrientes en
ecosistemas de humedal_

Edwin Hes obtuvo una maestría en ciencias por la Universidad de Wageningen en 1998, especializándose en ciencia y tecnología ambientales. De 1999 a 2002 trabajó para el Ministerio de Economía de los Países Bajos como consultor público en becas de investigación europeas.

Sus principales intereses de investigación durante los últimos años han sido el uso sostenible de los humedales y la modelación de los flujos de nutrientes en los ecosistemas de humedales, especialmente en los humedales de papiro en África Oriental. Está interesado en el uso de modelos para estudiar las funciones y servicios de los ecosistemas y evaluar los sistemas socioecológicos.

En el Instituto para la Educación del Agua (IHE Delft), coordinó el programa de Ciencias Ambientales de 2012 a 2016 y el programa de titulación conjunta en Limnología y Manejo de Humedales desde 2012 hasta la actualidad. Ha estado involucrado en actividades de enseñanza y creación de capacidades en los campos de Análisis de Sistemas Ambientales, Modelación Ambiental y Manejo de Humedales.

En sus actividades educativas trabaja con institutos de Kenia (Universidad de Egrton), Austria (BOKU), México (LANCIS-IE-UNAM) y Países Bajos (WUR-CDI). Desde 2007, Edwin ha trabajado en proyectos de investigación y desarrollo de capacidades en más de 20 países de Asia, África y América Latina con un enfoque en el uso sostenible de los humedales. Edwin es un facilitador y moderador experimentado en un entorno de desarrollo internacional.

<https://www.un-ihe.org/edwin-hes>
