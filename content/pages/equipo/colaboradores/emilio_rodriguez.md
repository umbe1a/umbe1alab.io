---
title: Dr. Emilio Rodríguez-Izquierdo
role: Colaborador
slug: equipo/emilio_rodriguez
headshot: /media/emilio_rodriguez.png
status: hidden
template: perfil
english: https://en.umbela.org/partner/emilio_rodriguez/
---

_Sostenibilidad, modelación socio-ecológica, resiliencia, instrumentos de política
pública ambiental (evaluación de impacto ambiental y ordenamiento ecológico) y
procesos participativos en la gestión de recursos naturales_

Emilio es Doctor en C. de la Sostenibilidad por la UNAM y Candidato a Investigador Nacional del SNI. 

En su doctorado, desarrolló un enfoque metodológico para determinar la capacidad de carga de embarcaciones de observación de ballena gris en la Laguna Ojo de Liebre, en BCS. En 2020, estuvo como investigador posdoctoral en el IIS-UNAM en el proyecto ¨Crisis Ambiental en México y Desigualdad¨. En este proyecto llevó a cabo análisis de datos de salud, acceso al agua y saneamiento, y su relación con la desigualdad, en tres municipios del estado de Chiapas, así como las implicaciones de todo ello para los ODS en México.

Ha trabajado en el LANCIS-IE-UNAM como coordinador técnico del desarrollo y validación de indicadores de la Estrategia de Resiliencia de la Ciudad de México y como técnico en medio ambiente donde participó en el desarrollo de insumos técnicos para instrumentos de planeación ambiental, como el Programa de Ordenamiento Ecológico Marino y Regional del Pacífico Norte de la SEMARNAT. Emilio tiene experiencia como consultor especialista en evaluación de impacto ambiental. En 2012 tuvo una breve colaboración voluntaria con la DGPAIRS de la SEMARNAT, donde participó en la integración de información para la formulación de la Política Nacional de Humedales y en el análisis de legislación relacionada con la zona costera en países Latinoamericanos. Como parte de su maestría en Estudios Ambientales por la Victoria University of Wellington de Nueva Zelanda desarrolló una tesis sobre los procesos participativos en la gestión del Parque Nacional Cordillera Azul, en Perú. 

Actualmente, Emilio es consultor en Ithaca Environmental donde está involucrado en diversos proyectos con enfoques novedosos (desde temas sobre el carbono azul en México a la incorporación de la vulnerabilidad al cambio climático en los atlas de riesgo).


<https://www.linkedin.com/in/emilio-rodríguez-izquierdo-mx>

