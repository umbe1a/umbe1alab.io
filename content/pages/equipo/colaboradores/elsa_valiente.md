---
title: M. en C. Elsa Valiente Riveros
role: Colaboradora
slug: equipo/elsa-valiente
headshot: /media/Elsa_Valiente.jpeg
status: hidden
template: perfil
english: https://en.umbela.org/equipo/elsa-valiente/
---

_Restauración de sistemas naturales, trabajo participativo, ciencia ciudadana_

Con estudios en Ciencias Biológicas y orientación en restauración ecológica, Elsa se ha desempeñado en los últimos 20 años en el trabajo participativo y de ciencia ciudadana, con productores agrícolas, estudiantes desde nivel básico a profesional, y con público en general, con la finalidad de enseñar los elementos y relaciones de los sistemas naturales y el impacto que nuestras acciones cotidianas, productivas y turísticas, ejercen en ellos.

Elsa ha gestionado y coordinado apoyos y colaboraciones  para el estudio del sistema productivo y de la calidad del agua en las chinampas de Xochimilco, que derivó en la capacitación a productores agrícolas de la subcuenca de Xochimilco, Milpa Alta y Tláhuac en técnicas de conservación de suelo, agroecología y agrobiodiversidad, y en la habilitación de infraestructura verde para coadyuvar en una mejora a la calidad del agua y de sus productos agrícolas. 

La restauración de los sistemas naturales que proveen recursos de uso alimenticio y medicinal, conlleva también la capacitación a los usuarios para una actividad productiva sustentable, lo cual se realizó también con el apoyo de voluntarios de empresas relevantes en el ámbito corporativo. Elsa considera que para la conservación de nuestros sistemas naturales y su biodiversidad, es fundamental proporcionar el conocimiento a través de distintos métodos, ya sean convencionales, participativos, o expresiones artísticas que muevan sentimientos y deseos por un futuro mejor.

