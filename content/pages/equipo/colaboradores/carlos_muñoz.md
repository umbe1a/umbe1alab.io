---
title: Dr. Carlos Muñoz Piña
role: Colaborador
slug: equipo/carlos_munoz
headshot: /media/Carlos_Munoz.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/carlos_munoz/
---

_Investigación económica-ambiental, en especial el diseño de instrumentos económicos_

Carlos Muñoz se ha dedicado a la investigación económica orientada a resolver retos 
ambientales y de recursos naturales.  En particular ha trabajado en el 
análisis para el diseño, negociación y evaluación de políticas públicas. 
Su énfasis ha sido en los instrumentos económicos para temas de servicios 
ambientales, economía circular, eficiencia energética, impuestos ambientales, 
mercados energéticos, energías limpias y mitigación y adaptación al 
Cambio Climático.  Varios de los proyectos en que ha trabajado se han 
convertido en iniciativas de ley, programas y proyectos en diversos países, 
publicando en revistas especializadas algunos de los análisis que han servido 
de respaldo a los mismos. 

Carlos Muñoz estudió Economía en el Instituto Tecnológico Autónomo de México (ITAM) 
en la Ciudad de México, la Maestría en Economía Ambiental por el University College London 
en el Reino Unido y el Doctorado en Economía de los Recursos Naturales en la 
Universidad de California en Berkeley.  Ha sido profesor-investigador en el 
Instituto Tecnológico Autónomo de México, la Universidad Iberoamericana y la 
Universidad del Medio Ambiente, y continúa impartiendo cursos y seminarios en 
estas y otras instituciones en México, EUA y el resto de Latinoamérica. 

A lo largo del tiempo, el Dr. Muñoz Piña ha sido investigador y coordinador de 
investigación en diversos centros gubernamentales y académicos, entre ellos el 
Instituto Nacional de Ecología y Cambio Climático (INECC) de la Secretaría de 
Medio Ambiente y Recursos Naturales del Gobierno de México, donde su equipo 
diseñó el sistema de Pago por Servicios Ambientales de los Bosques, en el 
Centro Mario Molina, donde su equipo diseñó el esquema del Impuesto al Carbono 
para México y realizó la investigación para el Mercado de Energías Limpias y el 
Mercado de Carbono.  Fue Economista en Jefe para Forest Trends y ha colaborado 
en el diseño de instrumentos económico-ambientales con el Banco Mundial y otros 
bancos de desarrollo multilaterales, con el USFS y la OCDE, entre otros.  
De 2004 a 2008 fue director de Política de Ingresos en la Secretaría de Hacienda 
y Crédito Pública, trabajando en temas de impuestos a combustibles y precios de 
energía, antes de unirse al programa global de Ciencia e Investigación para WRI Global. 

<https://www.researchgate.net/profile/Carlos-Munoz-Pina>
