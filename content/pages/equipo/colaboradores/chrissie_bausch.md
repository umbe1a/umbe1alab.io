---
title: Dra. Chrissie Bausch
role: Colaboradora
slug: equipo/chrissie_bausch
headshot: /media/chrissie_bausch.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/chrissie_bausch/
---


Chrissie es una científica social interdisciplinaria interesada en cómo la gobernanza y las políticas públicas
afectan la sostenibilidad y la equidad social. Ha realizado investigaciones sobre gestión de recursos naturales
gestión, cambio de uso de suelo, sistemas alimentarios, vivienda para poblaciones vulnerables, involucramiento de actores sociales, y adaptación al cambio climático.


C. Bausch tiene un doctorado en sostenibilidad de la Universidad Estatal de Arizona (ASU). 

Actualmente se desempeña como
analista científica en el Instituto Morrison de Políticas Públicas de ASU.


<https://www.linkedin.com/in/juliacbausch/>



