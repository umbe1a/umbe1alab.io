---
title: Dr. J. Mario Siqueiros-García
role: Colaborador
slug: equipo/mario_siqueiros
headshot: /media/Mario_Siqueiros.jpg
status: hidden
template: perfil
english: https://en.umbela.org/partner/mario_siqueiros/
---

_Ciencias sociales computacionales, sistemas complejos, ciencias de la sostenibilidad_


Investigador del Instituto de Investigaciones en Matemáticas Aplicadas y en Sistemas (IIMAS) 
de la UNAM en el Campus Yucatán, México.  Mario tiene experiencia en antropología y un
doctorado en Filosofía de la Ciencia y la Tecnología, con especialidad en Filosofía de la Biología. Colabora con investigadores de la Escuela de
Sostenibilidad en la Universidad Estatal de Arizona (ASU), el Instituto de Estudios del Desarrollo (IDS) y
el STEPS Center de la Universidad de Sussex y el proyecto de investigación independiente BeAnotherLab.
Mario es miembro y fundador del Colectivo Transdisciplina, Arte y Cognición (TACo). Durante los últimos tres años, ha sido co-organizador de la sesión especial ALife & Society, como parte de la conferencia ALife.

Su trabajo más reciente se centra en temas de sostenibilidad y sistemas socio-ecológicos.
En particular, le interesa el papel de la afectividad en el sentido de agencia vinculado
a problemas socio-ecológicos. Para ello, trabaja en la articulación de diferentes
herramientas cuantitativas como el análisis de redes complejas y herramientas cualitativas como
entrevistas a profundidad.
