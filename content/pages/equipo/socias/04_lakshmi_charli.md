---
title: Dra. Lakshmi Charli Joseph
role: Asesora Científica
slug: equipo/lakshmi_charli
headshot: /theme/images/equipo/lakshmi_charli.png
status: hidden
template: perfil
english: https://en.umbela.org/team/lakshmi_charli/
---

Bióloga de la UNAM, con una Maestría en Derecho, Gestión y Política
Ambientales (UAX) y una segunda sobre Planeación y Gestión Ambiental
en el Instituto para la Educación del Agua (IHE-Delft Holanda), con
ambos trabajos de tesis enfocados en política y gestión de cuencas
mexicanas. Es doctora en Ciencias de la Sostenibilidad (UNAM) con la investigación titulada “Promoviendo
Vías de Transformación hacia la Sostenibilidad a través de la agencia colectiva: el Laboratorio de Transformación en el sistema socio-ecológico de Xochimilco”.

Durante los últimos doce años ha estado involucrada en proyectos
educativos y de fortalecimiento de capacidades relacionados con el manejo
de humedales, la gobernanza del agua y las ciencias de la
sostenibilidad. Actualmente trabaja como
Técnica Académica Titular A en el Laboratorio Nacional de Ciencias de
la Sostenibilidad, Instituto de Ecología (LANCIS-IE), UNAM en
proyectos de investigación relacionados al diseño y facilitación de
procesos participativos que fomenten transformaciones hacia la
sostenibilidad en México.

Es parte de la red internacional Pathways, donde co-coordinó el nodo
de América del Norte, uno de los seis nodos que conforman el Consorcio
Global de Vías hacia la Sostenibilidad, del Centro STEPS, Reino
Unido. 


Para conocer más sobre sus proyectos y publicaciones:
<http://lancis.ecologia.unam.mx/personal/lakshmi>.
