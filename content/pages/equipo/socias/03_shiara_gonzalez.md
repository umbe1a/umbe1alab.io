---
title: Dra. Shiara González Padrón
role: Co-fundadora
slug: equipo/shiara_gonzalez
headshot: /theme/images/equipo/shiara_gonzalez.png
status: hidden
template: perfil
english: https://en.umbela.org/team/shiara_gonzalez/
---

Licenciada en Medicina Veterinaria por la Universidad Centroccidental
Lisandro Alvarado (UCLA), Venezuela, de donde es originaria. Vino a
México por primera vez en 2010, donde realizó prácticas profesionales
en la Dirección General de Vida Silvestre de la SEMARNAT, y volvió en
2011 para iniciar la Maestría en Ciencias dentro del Departamento de
Fauna Silvestre de la Facultad de Medicina Veterinaria de la
UNAM. 

Doctora en Ciencias de la Sostenibilidad de la UNAM. Su
investigación se centró en los cambios ocurridos a partir de la
introducción de sistemas de captación de agua de lluvia en dos
comunidades indígenas (Wixáritari) en Jalisco, México. 

Desde hace diez años colabora con diferentes organizaciones de la
sociedad civil (Isla Urbana, Proyecto ConcentrArte, Desarrollo Rural
Sustentable Lu’um e IRRI México) para atender diversas necesidades en
comunidades indígenas marginadas. En los últimos años se ha
desempeñado como coordinadora de campo y apoyado en investigación,
evaluación y gestión de recursos del área de eco-tecnologías en el
proyecto Ha Ta Tukari (agua nuestra vida). Su principal área de
interés es la investigación de los procesos de cambio y transformación
que ocurren desde la transdisciplina para impulsar los sistemas
socio-ecológicos hacia una sostenibilidad ambiental, bienestar humano
y justicia social en México y países del Sur Global.
