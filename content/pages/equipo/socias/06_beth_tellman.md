---
title: Dra. Beth Tellman
role: Asesora Científica
slug: equipo/beth_tellman
status: hidden
headshot: /theme/images/equipo/beth_tellman.png
template: perfil
english: https://en.umbela.org/team/beth_tellman/
---

Desde una perspectiva de geografía humano-ambiental, Beth busca
abordar las causas y consecuencias del cambio ambiental global en
poblaciones vulnerables, con un enfoque en el riesgo de acceso al
agua, inundaciones y el cambio de uso de la tierra.

Sus estudios en campo se han enfocado en México y Centroamérica. Es
cientifica post-doctoral en el Earth Institute de la Universidad de
Columbia, y del Proyecto ACToday (“Adaptación de la agricultura al
clima hoy, para el mañana”) en Bangladesh.

Es cofundadora de Cloud to Street, una corporación de beneficio
público que aprovecha los datos de teledetección para construir
sistemas de monitoreo y mapeo de inundaciones para países de ingresos
bajos y medianos. Doctora en Geografía por la Universidad Estatal de
Arizona. Actualmente es investigadora en la <a href="https://geography.arizona.edu/people/beth-tellman">Universidad de Arizona</a>

Para conocer más sobre sus publicaciones y proyectos:
<https://beth-tellman.github.io/>.
