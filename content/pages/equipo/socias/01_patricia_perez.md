---
title: Dra. Patricia Pérez Belmont
role: Directora General
slug: equipo/patricia_perez
headshot: /theme/images/equipo/patricia_perez.png
status: hidden
template: perfil
english: https://en.umbela.org/team/patricia_perez/
---

Patricia trabaja en el campo de las transformaciones para la sostenibilidad en contextos 
socio-ecológicos mediante la creación y facilitación de espacios y procesos participativos 
y colaborativos que buscan articular diversos saberes y culturas con el fin de fomentar 
la acción colectiva hacia futuros justos y sostenibles.

Patricia se formó como Bióloga con Maestría en Ciencias Biológicas, además realizó un doctorado 
en Ciencias de la Sostenibilidad; todos sus estudios los ha realizado en la Universidad Nacional 
Autónoma de México (UNAM). Patricia cuenta con una amplia experiencia en el diseño y coordinación 
de proyectos con actores sociales diversos utilizando enfoques inter y transdisciplinarios, 
herramientas participativas y pluralismo metodológico. Algunos de los temas trabajados en estos 
proyectos se relacionan con la creación de redes colaborativas para la sustentabilidad, implementación 
de prácticas productivas sustentables para la conservación, monitoreo de sistemas socio-ecológicos 
acuáticos, ciencia ciudadana, entre otros.

Como parte de su carrera y formación profesional, se ha dedicado a la enseñanza y alfabetización en adultos, 
así como enseñanza de la biología a nivel bachillerato y en licenciatura de Biología en la Facultad de
Ciencias de la UNAM. Patricia cuenta con publicaciones científicas y de divulgación, ha participado 
tanto en congresos nacionales como internacionales y como editor asociado en revistas indizadas. 
Por su experiencia y trayectoria fue invitada por el Departamento de Estado de Estados Unidos de América 
a través del buró de Asuntos Educativos y Culturales de su embajada en México para participar en el 
“International Visitor Leadership Program, Environmental Engagement and the Economy”. 

Patricia es actualmente la Directora de Umbela Transformaciones Sostenibles y además colabora en el 
Seminario Universitario de Sociedad, Medio Ambiente e Instituciones (SUSMAI) de la UNAM.

<https://www.researchgate.net/profile/Patricia-Perez-Belmont>

<https://www.linkedin.com/in/patricia-p%C3%A9rez-belmont-a261397a/>
