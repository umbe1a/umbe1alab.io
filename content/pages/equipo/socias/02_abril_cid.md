---
title: Dra. Abril Cid Salinas
role: Asesora científica
slug: equipo/abril_cid
status: hidden
headshot: /theme/images/equipo/abril_cid.png
template: perfil
english: https://en.umbela.org/team/abril_cid/
---

Cuenta con una maestría en ciencias, una especialidad en economía
ambiental y ecológica y, actualmente, es candidata a doctora en el
Posgrado en Ciencias de la Sostenibilidad, en la Universidad Nacional
Autónoma de México. Su proyecto de doctorado se dirige a analizar las
capacidades institucionales de gobiernos locales para responder y
adaptarse a fenómenos perturbadores y a tensiones socioambientales
crónicas en México. 


Además de su experiencia académica, se ha desempeñado como funcionario
público y consultor. En el sector público, se desempeñó como jefa de
departamento de políticas ambientales en la Secretaría de Medio
Ambiente y Recursos Naturales. Como consultor ha trabajado en
proyectos de evaluación de impacto ambiental, de análisis de
capacidades adaptativas locales para el cambio climático y en temas de
divulgación científica.


En su trayectoria académica y profesional, ha colaborado en el
desarrollo de insumos técnicos para instrumentos de planeación
ambiental, como el Programa de Ordenamiento Ecológico Marino Regional
del Pacífico Norte de la SEMARNAT, y para el Atlas Nacional de
Vulnerabilidad, como la Medición multidimensional de capacidad
institucional a nivel municipal que fomente la adaptación al cambio
climático en México, como parte de la colaboración entre el INECC,
PNUD y Transparencia Mexicana, A.C. Además, ha trabajado como
coordinador y colaborador técnico en el desarrollo de análisis de
vulnerabilidad en la Zona Patrimonial de Xochimilco, Milpa Alta y
Tláhuac y en la construcción del Sistema de Monitoreo, Reporte y
Verificación de la Estrategia de Resiliencia de la CDMX, como parte de
la colaboración entre la Agencia de Resiliencia de la CDMX y el
Laboratorio Nacional de Ciencias de la Sostenibilidad, del Instituto
de Ecología, de la UNAM.

Actualmente, Abril se desempeña como consultor en Ithaca Environmental
colaborando en proyectos de adaptación basada en ecosistemas
(componente de gobernanza en proyecto de Cuencas Verdes); de
fortalecimiento de capacidades de funcionarios públicos materia de
financiamiento climático (para una Junta Intermunicipal de Medio
Ambiente -AMUSUR- en Quintana Roo, para BANOBRAS y para
municipalidades en Ecuador); y para la creación de espacios de diálogo
multi-actor en materia de política pública costera.
