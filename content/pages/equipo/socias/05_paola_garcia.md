---
title: Dra. Paola Massyel García Meneses
role: Asesora Científica
slug: equipo/paola_garcia
headshot: /theme/images/equipo/paola_garcia.png
status: hidden
template: perfil
english: https://en.umbela.org/team/paola_garcia/
---

Bióloga egresada de la Facultad de Ciencias de la UNAM, cuenta con un
Doctorado en Ciencias de la Universidad de Plymouth, Reino Unido. 

Su trabajo se ha centrado principalmente en el estudio de procesos
socio-ecológicos a diferentes escalas temporales y espaciales, desde
el estudio de procesos como la polinización hasta procesos de cambio
global principalmente en sistemas Latinoamericanos.

Dentro de su carrera profesional ha ocupado puestos en el sector
público como Subdirectora de Conservación de las Comunidades
Biológicas y Adaptación al Cambio Climático, así como en
organizaciones como las Naciones Unidas y academia. Cuenta con
artículos publicados en revistas internacionales, reportes técnicos y
capítulos de libros.

Actualmente es investigadora del Laboratorio Nacional de Ciencias de
la Sostenibilidad (LANCIS), Instituto de Ecología de la UNAM donde
trabaja en proyectos en las áreas de vulnerabilidad, adaptación y
resiliencia a cambios globales, monitoreo y evaluación de sistemas
socio-ecológicos, así como la investigación de impacto y vinculación
de la ciencia promoviendo el involucramiento de otros sectores clave
para la toma de decisiones. Ha impartido clases en el Reino Unido,
Ecuador y México. Es representante dentro de los Comités de Trabajo de
Cambio Climático y Ciudades Sostenibles de la Red de Soluciones de
Desarrollo Sostenible (SDSN) de la ONU entre otros.

Para conocer más sobre sus publicaciones y proyectos:
<http://lancis.ecologia.unam.mx/personal/paola_garcia>.
