---
title: Contáctanos e involúcrate / Dona
subtitle: Contáctanos e involúcrate
slug: contacto
title_image: /theme/images/bg_contacto.jpg
fa_icon: fa-comments-o
english: https://en.umbela.org/contact/
---


<!-- form.123formbuilder.com script begins here --><script type="text/javascript" defer src="https://form.123formbuilder.com/embed/5896194.js" data-role="form" data-default-width="650px"></script><!-- form.123formbuilder.com script ends here -->


<center>
Agradecemos su donativo. 
Por favor llene después nuestro formulario de contacto para proporcionarle su recibo.
</center>

<center>
<form action="https://www.paypal.com/donate" method="post" target="_top">
<input type="hidden" name="hosted_button_id" value="3DFQ5X6RBYAA6" />
<input type="image" src="https://www.paypalobjects.com/en_US/MX/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
<img alt="" border="0" src="https://www.paypal.com/en_MX/i/scr/pixel.gif" width="2" height="2" />
</form>
</center>

<script type="text/javascript" id="ngos-ed-on-file-widget-script-17336c1f-917d-492b-bc53-225c95e103da">
 (function() {
 function async_load()
 { var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; var theUrl = 'http://www.ngosource.org/sites/default/files/ngos_ed_on_file_widget.js'; s.src = theUrl + ( theUrl.indexOf("?") >= 0 ? "&" : "?") + 'ref=' + encodeURIComponent(window.location.href); var embedder = document.getElementById('ngos-ed-on-file-widget-script-17336c1f-917d-492b-bc53-225c95e103da'); embedder.parentNode.insertBefore(s, embedder); }
 if (window.attachEvent)
 window.attachEvent('onload', async_load);
 else
 window.addEventListener('load', async_load, false);
 })();
 </script>
