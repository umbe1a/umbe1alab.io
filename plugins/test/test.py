from pelican import signals
import networkx as nx

def test(sender):
    socias = []
    for p in sender.hidden_pages:
        if p.relative_dir == 'pages/equipo/socias':
            socias.append(p)

    sender.context['socias'] = socias

    aliados = []
    for p in sender.hidden_pages:
        if p.relative_dir == 'pages/equipo/aliados':
            aliados.append(p)

    sender.context['aliados'] = aliados

    colaboradores = []
    for p in sender.hidden_pages:
        if p.relative_dir == 'pages/equipo/colaboradores':
            colaboradores.append(p)

    sender.context['colaboradores'] = colaboradores


    nodes = socias + colaboradores + aliados
    g = nx.barabasi_albert_graph(len(nodes) + 1, 2)
    # g = nx.erdos_renyi_graph(len(nodes) + 1, 0.2)
    # g = nx.generators.connected_watts_strogatz_graph(len(nodes) + 1, 2, 0.5)
    edges = g.edges()

    for p in sender.pages:
        if p.template == 'equipo':
            p.aliados = aliados
            p.socias = socias
            p.colaboradores = colaboradores
            p.edges = edges
            p.nodes = nodes            
            
            
def register():
    signals.page_generator_finalized.connect(test)
